package com.viettel.reeng.json;


import com.viettel.reeng.config.ReengCodeDesc;

public class ResponseObject {

    private int code;
    private String desc;
    private ResponseData data;
    //
    public ResponseObject(ReengCodeDesc.ReengCode reengCode){
        code = reengCode.getValue();
        desc =reengCode.getDesc();
    }

    public void setReengCode(ReengCodeDesc.ReengCode reengCode) {
        code = reengCode.getValue();
        desc = reengCode.getDesc();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData responseData) {
        this.data = responseData;
    }



}