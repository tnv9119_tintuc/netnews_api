package com.viettel.reeng.json;


import com.viettel.reeng.dao.sqlserver.entity.Categories;
import com.viettel.reeng.entity.News;
import com.viettel.reeng.entity.Objects;

import java.util.List;

/**
 */
public class ResponseData {
	private List<News> newsList;
	private List<Categories> categoriesList;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	private  News news;


	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	public List<Categories> getCategoriesList() {
		return categoriesList;
	}

	public void setCategoriesList(List<Categories> categoriesList) {
		this.categoriesList = categoriesList;
	}
}
