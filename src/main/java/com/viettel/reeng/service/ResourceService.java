package com.viettel.reeng.service;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class ResourceService {
	@Autowired
	protected MessageSource resource;
	private static final Logger logger = LogManager.getLogger(ResourceService.class);

	public String getMessage(String key, String language, String country) {
		try {
			if (!StringUtils.isEmpty(language) && !StringUtils.isEmpty(country)) {
				if ("vn".equalsIgnoreCase(language))
					language = "vi";
				if (language.indexOf("-") > 0) {
					language = language.split("-")[0];
				}
				return resource.getMessage(key, null, new Locale(language, "VN"));
			} else {
				return resource.getMessage(key, null, new Locale("en", "VN"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("ResourceService", e);
			return resource.getMessage(key, null, new Locale("en", "VN"));
		}

	}

	public String getMessage(String key, String language) {
		return getMessage(key, language, "VN");
	}

	public String getMessage(String key) {
		return resource.getMessage(key, null, new Locale("vi", "VN"));
	}
}
