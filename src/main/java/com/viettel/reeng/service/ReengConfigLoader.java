package com.viettel.reeng.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;

/**
 * 
 * @author nhungnt19
 *
 */

public class ReengConfigLoader {
	private static Logger log = LogManager.getLogger(ReengConfigLoader.class);

	private Resource location;

	/*private static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		}
	};*/

	public void loadConfig() {
		InputStream is = null;
		Reader reader = null;
		try {

			if (location.getFilename() != null) {
				EncodedResource resource = new EncodedResource(location,
						"UTF-8");
				reader = resource.getReader();
				is = resource.getInputStream();// not use utf-8
				Properties prop = new Properties();
				// prop.load(is);
				prop.load(reader);

				// genotp
//				ReengConfig.OTP_SMS_CONTENT = prop.getProperty("otpSmsContent",
//						ReengConfig.OTP_SMS_CONTENT);
//				try {
//					ReengConfig.LIMITED_TIME_S = Long.parseLong(prop
//							.getProperty("limitedTimeSd", ""
//									+ ReengConfig.LIMITED_TIME_S));
//				} catch (Exception e) {
//					log.error("Load LIMITED_TIME_S error: ", e);
//				}

//				try {
//					ReengConfig.CODE_EXP_TIME_S = Long.parseLong(prop
//							.getProperty("codeExpTimeS", ""
//									+ ReengConfig.CODE_EXP_TIME_S));
//				} catch (Exception e) {
//					log.error("Load CODE_EXP_TIME_S error: ", e);
//				}

				/*try {
					ReengConfig.BIRTHDAY_DEFAULT = Long.parseLong(prop
							.getProperty("birthdayDefault", ""
									+ ReengConfig.BIRTHDAY_DEFAULT));
				} catch (Exception e) {
					log.error("Load BIRTHDAY_DEFAULT error: ", e);
				}
				try {
					ReengConfig.OTP_MAX_SIZE = Integer.parseInt(prop
							.getProperty("otpMaxSize", ""
									+ ReengConfig.OTP_MAX_SIZE));
				} catch (Exception e) {
					log.error("Load OTP_MAX_SIZE error: ", e);
				}
				try {
					ReengConfig.GENDER_DEFAULT = Integer.parseInt(prop
							.getProperty("genderDefault", ""
									+ ReengConfig.GENDER_DEFAULT));
				} catch (Exception e) {
					log.error("Load GENDER_DEFAULT error: ", e);
				}
				ReengConfig.STATUS_DEFAULT = prop.getProperty("statusDefault",
						ReengConfig.STATUS_DEFAULT);*/
//				ReengConfig.SMSOUT_QUEUE_NAME = prop.getProperty(
//						"smsoutQueueName", ReengConfig.SMSOUT_QUEUE_NAME);
//				ReengConfig.SMSGW_ALIAS = prop.getProperty("otpAlias",
//						ReengConfig.SMSGW_ALIAS);
				// invite sms
//				ReengConfig.INVITE_SMS_CONTENT = prop.getProperty(
//						"inviteSmsContent", ReengConfig.INVITE_SMS_CONTENT);
//				ReengConfig.BASIC_DOMAIN = prop.getProperty("basicDomain",
//						ReengConfig.BASIC_DOMAIN);
//				ReengConfig.BASIC_CONTENT = prop.getProperty("basicContent",
//						ReengConfig.BASIC_CONTENT);
//				ReengConfig.BASIC_CHAT_DOMAIN = prop.getProperty(
//						"basicChatDomain", ReengConfig.BASIC_CHAT_DOMAIN);
//				ReengConfig.BASIC_FILE_DOMAIN = prop.getProperty(
//						"basicFileDomain", ReengConfig.BASIC_FILE_DOMAIN);

//				ReengConfig.NEWUSERNVLT = prop.getProperty("newuserNVLT",
//						ReengConfig.NEWUSERNVLT);
//				ReengConfig.ALI			ASNVLT = prop.getProperty("aliasNVLT",
//						ReengConfig.ALIASNVLT);
//				ReengConfig.NEWUSERNOTIFY = prop.getProperty("newuserNotify",
//						ReengConfig.NEWUSERNOTIFY);
//				ReengConfig.NEWUSERNOTVIETTEL = prop.getProperty(
//						"newuserNotViettel", ReengConfig.NEWUSERNOTVIETTEL);
//				ReengConfig.NEWUSERNOTIFYNOTVIETTEL = prop.getProperty(
//						"newuserNotifyNotViettel",
//						ReengConfig.NEWUSERNOTIFYNOTVIETTEL);

				/*try {
					ReengConfig.ENABLE_WHITELIST = Integer.parseInt(prop
							.getProperty("enableWhitelist", ""
									+ ReengConfig.ENABLE_WHITELIST));
				} catch (Exception e) {
					log.error("Load ENABLE_WHITELIST error: ", e);
				}*/

//				ReengConfig.DESC_WHITELIST = prop.getProperty("descWhitelist",
//						ReengConfig.DESC_WHITELIST);
//				ReengConfig.DESC_INVITE_WHITELIST = prop.getProperty(
//						"descInviteWhitelist",
//						ReengConfig.DESC_INVITE_WHITELIST);

				// otp
				/*try {
					ReengConfig.CONTACT_THREAD_POOL_SIZE = Integer
							.parseInt(prop.getProperty("contactThreadPoolSize",
									"" + ReengConfig.CONTACT_THREAD_POOL_SIZE));
				} catch (Exception e) {
					log.error("Load CONTACT_THREAD_POOL_SIZE error: ", e);
				}*/

				// funquiz
				/*ReengConfig.QUESTION_EXP_CONTENT = prop.getProperty(
						"quesExpContent", ReengConfig.QUESTION_EXP_CONTENT);
				ReengConfig.QUESTION_INVALID_CONTENT = prop.getProperty(
						"quesInvalidContent",
						ReengConfig.QUESTION_INVALID_CONTENT);

				// sticker
				ReengConfig.STICKER_PATH = prop.getProperty("sticker.path",
						ReengConfig.STICKER_PATH);
				ReengConfig.STICKER_STORE_NAME = prop.getProperty(
						"sticker.store.name", ReengConfig.STICKER_STORE_NAME);
				ReengConfig.STICKER_STORE_AVATAR = prop.getProperty(
						"sticker.store.avatar",
						ReengConfig.STICKER_STORE_AVATAR);
				ReengConfig.STICKER_STORE_ICON = prop.getProperty(
						"sticker.store.icon", ReengConfig.STICKER_STORE_ICON);*/

				// promotion
				/*try {
					ReengConfig.PROMOTION_WOMAN_DAY_ENABLE = Integer
							.parseInt(prop
									.getProperty(
											"promotionWomanDayEnable",
											""
													+ ReengConfig.PROMOTION_WOMAN_DAY_ENABLE));
				} catch (Exception e) {
					log.error("Load PROMOTION_WOMAN_DAY_ENABLE error: ", e);
				}

				try {
					ReengConfig.PROMOTION_WOMAN_DAY_NUMBER = Integer
							.parseInt(prop
									.getProperty(
											"promotionWomanDayNumber",
											""
													+ ReengConfig.PROMOTION_WOMAN_DAY_NUMBER));
				} catch (Exception e) {
					log.error("Load PROMOTION_WOMAN_DAY_NUMBER error: ", e);
				}*/

				/*try {
					String startDay = prop.getProperty(
							"promotionWomanDayStart", "");
					String endDay = prop
							.getProperty("promotionWomanDayEnd", "");
					if (startDay != null && !startDay.isEmpty()) {
						ReengConfig.PROMOTION_WOMAN_DAY_START = formatter.get()
								.parse(startDay);
					}
					if (endDay != null && !endDay.isEmpty()) {
						ReengConfig.PROMOTION_WOMAN_DAY_END = formatter.get()
								.parse(endDay);
					}
				} catch (Exception e) {
					log.error("Load PROMOTION_WOMAN_DAY_xxx error: ", e);
				}

				ReengConfig.PROMOTION_WOMAN_DAY_MSG = prop.getProperty(
						"promotionWomanDayMsg",
						ReengConfig.PROMOTION_WOMAN_DAY_MSG);

				// vqmm
				ReengConfig.VQMM_EXPIRED_MSG = prop.getProperty("vqmmExpMsg",
						ReengConfig.VQMM_EXPIRED_MSG);
				ReengConfig.VQMM_INVALID_VTNUMBER = prop.getProperty(
						"vqmmInvalidVTNumber",
						ReengConfig.VQMM_INVALID_VTNUMBER);
				ReengConfig.VQMM_OFF_MSG = prop.getProperty("vqmmOffMsg",
						ReengConfig.VQMM_OFF_MSG);*/
				/*try {
					ReengConfig.VQMM_NUMBER = Integer.parseInt(prop
							.getProperty("vqmmNumber", ""
									+ ReengConfig.VQMM_NUMBER));
				} catch (Exception e) {
					log.error("Load VQMM_NUMBER error: ", e);
				}

				try {
					ReengConfig.VQMM_ID = Integer.parseInt(prop.getProperty(
							"vqmmId", "" + ReengConfig.VQMM_ID));
				} catch (Exception e) {
					log.error(e);
				}*/

				/*try {
					ReengConfig.INTER_OTP_BATCH = Integer.parseInt(prop
							.getProperty("interOtpBatch", ""
									+ ReengConfig.INTER_OTP_BATCH));
				} catch (Exception e) {
					log.error("Load INTER_OTP_BATCH error: ", e);
				}*/
				
				/*ReengConfig.INTER_OTP_MSG = prop.getProperty("interOtpMsg",
						ReengConfig.INTER_OTP_MSG);*/
				
				
				/******************** Birthday *****************************/
				/*ReengConfig.BIRTHDAY_PUSH_TIME = prop.getProperty("birthdayPushTime",
						ReengConfig.BIRTHDAY_PUSH_TIME);
				
				ReengConfig.BIRTHDAY_PUSH_TIME_WEEKEND = prop.getProperty("birthdayPushTimeWeekend",
						ReengConfig.BIRTHDAY_PUSH_TIME_WEEKEND);
				
				*//************* Birthday ****************//*
				ReengConfig.OFFICAL_QUEUE_NAME = prop.getProperty("officalQueueName",
						ReengConfig.OFFICAL_QUEUE_NAME);
						
				ReengConfig.BIRTHDAY_CONTENT = prop.getProperty("birthdayContent",
						ReengConfig.BIRTHDAY_CONTENT);
				ReengConfig.CONTACT_BIRTHDAY_CONTENT = prop.getProperty(
						"contactBirthdayContent", ReengConfig.CONTACT_BIRTHDAY_CONTENT);
				ReengConfig.CONTACT_BIRTHDAY_CONTENT_NEW = prop.getProperty(
						"newContactBirthdayContent", ReengConfig.CONTACT_BIRTHDAY_CONTENT_NEW);
				
				//check revision
				ReengConfig.CLIENT_TYPE_ANDROID = prop.getProperty("clientTypeAndroid",
						ReengConfig.CLIENT_TYPE_ANDROID);*/
				
				
				/*try {
					ReengConfig.THRESHOLD_ANDROID_REVISION = Integer.parseInt(prop.getProperty(
							"androidRevision", String.valueOf(ReengConfig.THRESHOLD_ANDROID_REVISION)));
				} catch (Exception e) {
					log.error("Cannot load androidRevision config", e);
				}	
				
				
				ReengConfig.CLIENT_TYPE_IOS = prop.getProperty("clientTypeIos",
						ReengConfig.CLIENT_TYPE_IOS);
				
				try {
					ReengConfig.THRESHOLD_IOS_REVISION = Integer.parseInt(prop.getProperty(
							"iosRevision", String.valueOf(ReengConfig.THRESHOLD_IOS_REVISION)));
				} catch (Exception e) {
					log.error("Cannot load iosRevision config", e);
				}
				
				
				
				ReengConfig.CLIENT_TYPE_WINPHONE = prop.getProperty("clientTypeWindowPhone",
						ReengConfig.CLIENT_TYPE_WINPHONE);
				
				
				try {
					ReengConfig.THRESHOLD_WINPHONE_REVISION = Integer.parseInt(prop.getProperty(
							"windowphoneRevision", String.valueOf(ReengConfig.THRESHOLD_WINPHONE_REVISION)));
				} catch (Exception e) {
					log.error("Cannot load windowphoneRevision config", e);
				}*/

				
			}

		} catch (IOException ex) {
			log.error("Can not load app configure from " + location, ex);

		} catch (NumberFormatException ex) {
			log.error("Can not load app configure from " + location, ex);

		} catch (Exception ex) {
			log.error("Can not load app configure from " + location, ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException ex) {
				log.error(ex.getMessage(), ex);
			}
		}
	}

	public Resource getLocation() {
		return location;
	}

	public void setLocation(Resource location) {
		this.location = location;
	}

}
