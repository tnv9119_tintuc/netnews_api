package com.viettel.reeng.dao.sqlserver.entity;

public class Categories {
    private  int Id;
    private  int Pid;
    private  int Vt;
    private  String Name;
    private  String Description;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getPid() {
        return Pid;
    }

    public void setPid(int pid) {
        Pid = pid;
    }

    public int getVt() {
        return Vt;
    }

    public void setVt(int vt) {
        Vt = vt;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}