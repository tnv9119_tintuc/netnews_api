package com.viettel.reeng.config;

public class Constants {
    public static final String ImageDBPath = "http://mcnews.mediavip.netnews.vn:8080/netnews/";
    public static final String ImageTinTopPath = "http://125.235.37.50/";
    public static final String ImageDBPathVIP = "http://mcnews.mediavip.netnews.vn:6868/netnews/";
    public static final String isVip = "1";
    public static final String video_config_cdn = "2003335";
    public static final String video_cdn = "http://mcnews2.mediavip.netnews.vn/news/netnews/archive/";
    public static final String video_cdn_normal = "http://mcnews2.media.netnews.vn/news/netnews/archive/";
    public static final String ImageAppVipDB = "http://media3.netnews.vn:6868/archive/";
    public static final String ImageDBPathVIPNew = "http://mcnews.mediavip.netnews.vn:6868/netnews/";
    public static final String ImageDBPathOld = "http://mcnews.mediavip.netnews.vn:8080/netnews/";
    public static final String ImageDBPathNew = "http://mcnews.mediavip.netnews.vn:8080/netnews/";
    public static final String max_id_img_old = "1358784";
    public static final String LinkWap = "http://netnews.vn/";

}
