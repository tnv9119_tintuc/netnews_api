package com.viettel.reeng.config;

import com.google.gson.JsonObject;

public class ReengCodeDesc {

    //

    public static enum ReengCode {

        //insert
        ERROR_INSERT(101),
        // music together room
        MUSIC_ROOM_BUSY(205), MUSIC_ROOM_TIMEOUT(206),
        //Too big file OR error type
        ERROR_SELECT_FILE(203),ERROR_BIG_FILE(204),ERROR_IS_EMPTY(207), ERROR_TOO_LONG(209),
        ERROR_UPLOAD_FAIL(-1),ERROR_UPLOAD_LARGE(-4),
        // funquiz
        QUESTION_EXPIRED(422), CORRECT_ANSWER(420), INCORRECT_ANSWER(421),

        // general
        NOT_FOUND(404), BAD_REQUEST_ERROR(400), NOT_AUTHORIZED(401), TOKEN_EXPIRED(419),

        // opt code
        /**
         * Generate code successful & Unsuccess
         */
        SUCCESSFUL(200),
        UNSUCCESSFUL(208),

        /**
         * Error: username is not phone number 405 not-allow
         */
        ERROR_INVALID_USERNAME(405),

        /**
         * WARN: not detect 3g viettel
         */
        NOT_DETECT3G(430),

        /**
         * Error: username is not phone number 405 not-allow
         */
        ERROR_FORBIDDEN_USERNAME(403),

        /**
         * Error: internal error
         */
        ERROR_INTERNAL(500),

        /**
         * Error: username is locked 550 permission denied
         */
        ERROR_LOCKED_USERNAME(550);

        private int value;

        ReengCode(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public String getDesc() {
            String result = null;
            switch (value) {
                case -1:
                    result = "Upload fail";
                    break;
                case -4:
                    result = "File upload too large";
                    break;
                case 101:
                    result = "Error insert DB";
                    break;
                case 403:
                    result = "Forbidden";
                    break;
                case 405:
                    result = "Username is not phone number";
                    break;
                case 550:
                    result = "Username is locked";
                    break;
                case 200:
                    result = "Successful";
                    break;
                case 203:
                    result = "Please select a file to upload";
                    break;
                case 204:
                    result = "Too big file OR error type";
                    break;
                case 205:
                    result = "Busy";
                    break;
                case 206:
                    result = "Timeout";
                    break;
                case 207:
                    result = "Is empty";
                    break;
                case 208:
                    result = "Unsuccessful";
                    break;
                case 209:
                    result = "Too Long";
                    break;
                case 500:
                    result = "Internal error";
                    break;
                case 404:
                    result = "User not found";
                    break;
                case 400:
                    result = "bad request";
                    break;
                case 401:
                    result = "not authorized";
                    break;
                case 419:
                    result = "token expired";
                    break;

                case 422:
                    result = "question expired";
                    break;
                case 420:
                    result = "correct answer";
                    break;
                case 421:
                    result = "incorrect answer";
                    break;

                case 430:
                    result = "not detect 3g viettel";
                    break;

                default:
                    result = "Error";

            }

            return result;

        }

        @Deprecated
        public JsonObject getJsonObj() {
            JsonObject obj = new JsonObject();

            obj.addProperty("errorCode", this.value);
            obj.addProperty("desc", this.getDesc());

            return obj;
        }

        @Deprecated
        public String getJsonString() {
            return getJsonObj().toString();
        }

        // tmp
        public JsonObject getOtpJsonObj() {
            JsonObject obj = new JsonObject();

            obj.addProperty("code", this.value);
            obj.addProperty("desc", this.getDesc());
            return obj;
        }

        public JsonObject getOtpJsonObj(String description) {
            JsonObject obj = new JsonObject();

            obj.addProperty("code", this.value);
            obj.addProperty("desc", description);
            return obj;
        }

        public String getOtpJsonString() {
            return getOtpJsonObj().toString();
        }

        public String getOtpJsonString(String description) {
            return getOtpJsonObj(description).toString();
        }
    }

}
