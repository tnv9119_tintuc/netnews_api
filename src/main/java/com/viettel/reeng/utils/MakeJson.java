package com.viettel.reeng.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class MakeJson {
	
	public MakeJson() {
		// TODO Auto-generated constructor stub
		gson = new Gson();
		jsonParser = new JsonParser();
		json = new JsonObject();
	}
	private JsonObject json;
	private Gson gson;
	private JsonParser jsonParser;
	
	public JsonObject addPropery(String property, String value){
		json.addProperty(property, value);
		return json;
	}
	public JsonObject addPropery(String property, Boolean value){
		json.addProperty(property, value);
		return json;
	}
	public JsonObject addPropery(String property, Number value){
		json.addProperty(property, value);
		return json;
	}
	public JsonObject addElement(String property, JsonElement value){
		json.add(property, value);
		return json;
	}
	public <T> JsonObject addArray(String property, List<T> lists){
		JsonElement element = gson.toJsonTree(lists, new TypeToken<List<T>>() {}.getType());
		json.add(property, element);
		return json;
	}
	public JsonObject addArray(String property, JsonArray array){
		json.add(property, array);
		return json;
	}
	
	public String convertToJson(Object obj){
		return gson.toJson(obj);
	}
	public <T> T convertJsonToClass(String input, Class<T> classT){
		return gson.fromJson(input, classT);
	}
	
	public JsonElement parse(String input){
		return jsonParser.parse(input);
	}
	public JsonObject getJson() {
		return json;
	}	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return json.toString();
	}
	public static void main(String[] args) {
		MakeJson json = new MakeJson();
		json.addPropery("u", "xxx");
		json.addPropery("y", "yyyy");
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
//		json.addArray("x", list);
		List<JsonResult> jsonResults = new ArrayList<JsonResult>();
		JsonResult jsonResult = new JsonResult(1, "1");
		jsonResults.add(jsonResult);
		jsonResult = new JsonResult(2, "2");
		jsonResults.add(jsonResult);
		json.addArray("x", jsonResults);
		String aa = json.toString();
		JsonObject  jsonObject = json.getJson();
		System.out.println(aa);
		json = new MakeJson();
		json.addElement("u1", jsonObject);
		json.addPropery("y1", "yyyy");
		
		json.addArray("x1", list);
		System.out.println(json.toString());
	}
}
