package com.viettel.reeng.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.hadoop.hbase.util.Bytes;
import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

public class BytesUtils {

	private static final Logger logger = LogManager.getLogger(BytesUtils.class);

	public static byte[] toDigest(String s) {
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(Bytes.toBytes(s));
		} catch (NoSuchAlgorithmException e) {
			logger.error("NoSuchAlgorithmException ", e);
			 
		}
		return null;

	}

	public static byte[] binaryIncrementPos(byte[] value) {
		int i;
		byte[] bytes = Bytes.copy(value);
		for (i = bytes.length - 1; i >= 0; i--) {
			bytes[i]++;
			if (bytes[i] != 0)
				break;
		}
		return bytes;
	}
}
