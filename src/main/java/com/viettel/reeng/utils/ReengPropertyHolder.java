
/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.viettel.reeng.utils;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.viettel.security.PassTranformer;

public class ReengPropertyHolder extends PropertyPlaceholderConfigurer {

	private String encryptFlag = ".enc";

	@Override
	protected String convertProperty(String propertyName, String propertyValue) {
		if (encryptFlag != null && propertyName.contains(encryptFlag)) {
			try {
				logger.info("----------------------->PassTranformer:::::" + PassTranformer.decrypt(propertyValue));
				return PassTranformer.decrypt(propertyValue);
			} catch (Exception ex) {
				logger.error("Cannot decrypt: property=" + propertyName + ", value=" + propertyValue);
				return convertPropertyValue(propertyValue);
			}
		}
		return convertPropertyValue(propertyValue);
	}

	public void setEncryptFlag(String encryptFlag) {
		this.encryptFlag = encryptFlag;
	}
}
