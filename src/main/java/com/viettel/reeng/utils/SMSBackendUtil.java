	package com.viettel.reeng.utils;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SMSBackendUtil {

	private static final String ALPHA_NUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static String msisdnNormalize(String msisdn) {
		if (msisdn.startsWith("84") || msisdn.startsWith("+84") || msisdn.startsWith("0084")) {
			return msisdn;
		}

		if (msisdn.startsWith("0")) {
			return "84" + msisdn.substring(1, msisdn.length());
		}
		if (msisdn.startsWith("+509")) {
			return msisdn;
		}
		if (msisdn.startsWith("+")) {
			return msisdn;
		}
		return "84" + msisdn;
	}

	public static String msisdnFor888(String msisdn) {
		if (msisdn.startsWith("84") || msisdn.startsWith("+84") || msisdn.startsWith("0084")) {
			return "888" + msisdn;
		}

		if (msisdn.startsWith("0")) {
			return "888" + "84" + msisdn.substring(1, msisdn.length());
		}
		if (msisdn.startsWith("+509")) {
			return msisdn;
		}
		if (msisdn.startsWith("+")) {
			return msisdn;
		}
		return "88884" + msisdn;
	}

	public static String msisdnFor111(String msisdn) {
		if (msisdn.startsWith("84") || msisdn.startsWith("+84") || msisdn.startsWith("0084")) {
			return "111" + msisdn;
		}

		if (msisdn.startsWith("0")) {
			return "111" + "84" + msisdn.substring(1, msisdn.length());
		}
		if (msisdn.startsWith("+509")) {
			return msisdn;
		}
		if (msisdn.startsWith("+")) {
			return msisdn;
		}
		return "11184" + msisdn;
	}

	public static String genSessionID(int length) {
		StringBuffer sb = new StringBuffer(length);
		for (int i = 0; i < length; i++) {
			int ndx = (int) (Math.random() * ALPHA_NUM.length());
			sb.append(ALPHA_NUM.charAt(ndx));

		}
		return sb.toString();
	}

	public static String removeAccent(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replaceAll("đ", "d");
	}

	public static final ThreadLocal<SimpleDateFormat> formatter = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		}
	};
	private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz"
			+ "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();

	private static Random randGen = new Random();

	public static String randomString(int length) {
		if (length < 1) {
			return null;
		}
		// Create a char buffer to put random letters and numbers in.
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
		}
		return new String(randBuffer);
	}

	public static String toJson(Map<String, Object> map) {
		String jsonStr = null;
		Gson gson = new GsonBuilder().create();
		jsonStr = gson.toJson(map);
		return jsonStr;

	}

}
