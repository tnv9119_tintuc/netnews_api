package com.viettel.reeng.utils;

import com.viettel.reeng.config.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

public class Utils {
    private static Random rnd = new Random();
    public static String sWebImageVip = Constants.ImageDBPathVIP;
    public static String sWebImageVipNew = Constants.ImageDBPathVIPNew;
    public static String sWebImage = Constants.ImageDBPathOld;
    public static String sWebImageNew = Constants.ImageDBPathNew;

    public static int Max_id_img_old = Integer.parseInt(Constants.max_id_img_old);


    public static String GenerateImage(Object Html) {
        return GenerateImage(Html.toString());
    }

    public static String GenRewriteURLImage(String lead_image, int id, int vip) {
        String link_image = "";

        try {
            if (lead_image.contains("http")) {
                link_image = lead_image;
            } else {
                //chuyển sang cdn
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= id && lead_image.contains(".mp4")) {
                    String archivePath = lead_image.substring(0, 8);
                    if (archivePath.contains("archive")) {
                        lead_image = lead_image.replace("/archive", "");
                        lead_image = lead_image.replace("archive", "");
                    }

                    int slashIndex = 0;
                    while (slashIndex != 1) {
                        slashIndex = lead_image.toLowerCase().indexOf("/");
                        if (slashIndex == 0)
                            lead_image = lead_image.substring(1);
                        else
                            slashIndex = 1;
                    }

                    if (vip == 1)
                        link_image = Constants.video_cdn + lead_image;
                    else
                        link_image = Constants.video_cdn_normal + lead_image;

                } else {
                    if (vip == 1) {
                        if (id == 0) {
                            link_image = Utils.sWebImageVip + lead_image;
                        } else {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageVipNew + lead_image;
                            else
                                link_image = Utils.sWebImageVip + lead_image;
                        }
                    } else {
                        if (id == 0) {
                            link_image = Utils.sWebImage + lead_image;
                        } else {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageNew + lead_image;
                            else
                                link_image = Utils.sWebImage + lead_image;
                        }
                    }
                }
            }
        } catch (Exception e) {
            //
        }


        return link_image;

    }


    public static String GenRewriteURLImage(String lead_image, int id) {
        String link_image = "";

        try {
            if (lead_image.contains("http://")) {
                link_image = lead_image;
            } else {
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= id && lead_image.contains(".mp4")) {

                    String archivePath = lead_image.substring(0, 8);
                    if (archivePath.contains("archive")) {
                        lead_image = lead_image.replace("/archive", "");
                        lead_image = lead_image.replace("archive", "");
                    }

                    int slashIndex = 0;
                    while (slashIndex != 1) {
                        slashIndex = lead_image.indexOf("/");
                        if (slashIndex == 0)
                            lead_image = lead_image.substring(1);
                        else
                            slashIndex = 1;

                    }
                    link_image = Constants.video_cdn + lead_image;
                } else {
                    if (id >= Utils.Max_id_img_old)
                        link_image = Utils.sWebImageVipNew + lead_image;
                    else
                        link_image = Utils.sWebImageVip + lead_image;
                }
            }
        } catch (Exception e) {
            //
        }

        return link_image;
    }

//    private String removeDivHidden(String html) {
//        String pattern = @ "\A(?:<div\s*[^s]*.*style\s*=\s*\""[ ^ ""]*display\s *:\s * none[ ^ ""]*\""[ ^>]*>(. |\s)*?<\/
//        div >)\Z ";
//        return Regex.Replace(html, pattern, String.Empty, RegexOptions.IgnoreCase);
//    }

//    private static String GenerateImage(String Html) {
//        // String s = ConfigurationSettings.AppSettings["URL_NEW"];
//        String maxWidth = "320";// ConfigurationSettings.AppSettings["IMAGE_MAX_WIDTH"];
//        // String ashx = ConfigurationSettings.AppSettings["IMG_ASHX"];
//        String sWebImage = Constants.ImageDBPath;
//        // Html = Regex.Replace(Html, VTMLib.Variables.sOldWebRoot, VTMLib.Variables.sWebRoot, RegexOptions.IgnoreCase);
//        Html = Regex.Replace(Html, "/cms/", sWebImage, RegexOptions.IgnoreCase);
//        String pattern = @ "\""[ ^ ""]*/image / ";    // khi su dung @ thi " " = "
//        // Html = Regex.Replace(Html, pattern, "\"" + s, RegexOptions.IgnoreCase);
//
//        pattern = @ "" "(\.{1,2}/)+archive/images";
//
//
//        Html = Regex.Replace(Html, pattern, "\"" + sWebImage + "archive/images", RegexOptions.IgnoreCase);
//
//
//        Html = Html.replace(".jpg", "wap_" + maxWidth + ".jpg");
//        Html = Html.replace(".gif", "wap_" + maxWidth + ".gif");
//        Html = Html.replace(".png", "wap_" + maxWidth + ".png");
//        Html = Html.replace(".JPG", "wap_" + maxWidth + ".JPG");
//        Html = Html.replace(".GIF", "wap_" + maxWidth + ".GIF");
//        Html = Html.replace(".PNG", "wap_" + maxWidth + ".PNG");
//
//        //them moi 10/04/2012 vudoan2
//        Html = Html.replace("http://tinngan.vn", "http://m.tinngan.vn");
//        Html = Html.replace("http://wap.tinngan.vn", "http://m.tinngan.vn");
//        Html = Html.replace("http://m.tinngan.vn/viewNew.aspx", "http://m.tinngan.vn/view.aspx");
//
//
//        Html = Html.replace("<input>", "");
//        Html = Html.replace("&lt;input&gt;", "");
//
//        return Html;
//    }


    public static String VNFormatDate(Date date) {
        String sDate = "";
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
        String dayOfWeek = simpleDateformat.format(date);
        try {


            switch (dayOfWeek) {
                case "Monday":
                    sDate = "Thứ hai, ";
                    break;
                case "Tuesday":
                    sDate = "Thứ ba, ";
                    break;
                case "Wednesday":
                    sDate = "Thứ tư, ";
                    break;
                case "Thursday":
                    sDate = "Thứ năm, ";
                    break;
                case "Friday":
                    sDate = "Thứ sáu, ";
                    break;
                case "Saturday":
                    sDate = "Thứ bảy, ";
                    break;
                case "Sunday":
                    sDate = "Chủ nhật, ";
                    break;
            }


        } catch (Exception e) {
            e.getMessage();
        }
        sDate += " " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
        return sDate;
    }


    public static String GetDate() {
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
        String dayOfWeek = simpleDateformat.format(new Date());
        switch (dayOfWeek) {
            case "Sunday":
                return ("Chủ nhật, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Monday":
                return ("Thứ hai, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Tuesday":
                return ("Thứ ba, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Wednesday":
                return ("Thứ tư, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Thursday":
                return ("Thứ năm, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Friday":
                return ("Thứ sáu, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

            case "Saturday":
                return ("Thứ bảy, " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        }
        return "";
    }

    public static String GetDateFormat(Date date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").format(date);
        } catch (Exception e) {
            return "";
        }
    }


//    public static String getRandom() {
//        return rnd.Next(0x3b9ac9ff).ToString();
//    }


//    public static String KillChars(String strWords) {
//        String[] strArray = new String[]{"select", "drop", ";", "--", "insert", "delete", "xp_", "'"};
//        String input = strWords;
//        for (String str2 : strArray) {
//            input = Regex.Replace(input, str2, "", RegexOptions.IgnoreCase);
//        }
//        return input.replace("[", "");
//    }


    public static String ReFormatSlideText(String title) {
        int length = title.lastIndexOf('"');
        String str = title.substring(length + 1);
        if (length > 0) {
            title = title.substring(0, length) + "”" + str;
        }
        title = title.replace('"', '“');
        return title;
    }

    public static String ReFormatText(String title) {
        int length = title.lastIndexOf('"');
        String str = title.substring(length + 1);
        if (length > 0) {
            title = title.substring(0, length) + "”" + str;
        }
        title = title.replace('"', '“');
        return title;
    }


    public static String TrimCharSpec(Object input) {
        String str = (String) input;
        return str.replace("'", "\"");
    }

//    public static String ValidateBoxID(String input, String defaultValue) {
//        Pattern pattern = new Pattern("[^0-9_]");
//        if (!regex.IsMatch(input) && (input != "")) {
//            return input;
//        }
//        return defaultValue;
//    }

//    public static String ValidateDate(String input, String defaultValue) {
//        Regex regex = new Regex("[^0-9]");
//        if (!regex.IsMatch(input) && (input != "")) {
//            return input;
//        }
//        return defaultValue;
//    }

//    public static String ValidateInt(String input, String defaultValue) {
//        if (input != null) {
//            Regex regex = new Regex("[^0-9]");
//            if (!regex.IsMatch(input) && (input != "")) {
//                return input;
//            }
//        }
//        return defaultValue;
//    }

//    public static String GetValue(String name, String defaultValue) {
//        String res = (HttpContext.Current.Request.QueryString[name] == null ? defaultValue : HttpContext.Current.Request.QueryString[name]);
//        res = ValidateXSS(res);
//        res = KillChars(res);
//        return res;
//    }

//    public static String ValidateXSS(String strWords) {
//        StringBuilder sb = new StringBuilder(HttpUtility.HtmlEncode(strWords));
//
//        String[] badChars = {"&lt;", "&gt;", "/", "script", "iframe"};
//        String newChars = sb.ToString();
//        foreach(String str in badChars)
//        {
//            //newChars = newChars.Replace(str, "");
//            newChars = Regex.Replace(newChars, str, "", RegexOptions.IgnoreCase);
//        }
//
//        return newChars;
//    }

    //// xoa cac ki tu dac biet trong input dau vao
    //public static String KillChars(String strWords)
    //{
    //    String[] badChars = { "xp_", ";", "--", "<", ">", "script", "iframe", "delete", "drop", "exec", "insert", "'" };
    //    String newChars = strWords;
    //    foreach (String str in badChars)
    //    {
    //        //newChars = newChars.Replace(str, "");
    //        newChars = Regex.Replace(newChars, str, "", RegexOptions.IgnoreCase);
    //    }
    //    newChars = newChars.Replace("[", "");

    //    return newChars;
    //}


}

