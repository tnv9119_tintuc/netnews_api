package com.viettel.reeng.utils;

import com.viettel.reeng.utils.ReengCodeDesc.ReengCode;

/**
 * replace by com.viettel.reeng.exception.ReengNotAuthorizedException
 * 
 * @author huybq10
 *
 */
@Deprecated
public class ReengNotAuthorizedException extends Exception {
	private ReengCode reengCode;

	public ReengNotAuthorizedException(ReengCode reengCode) {
		super(reengCode.getDesc());
		this.reengCode = reengCode;
	}

	public ReengCode getReengCode() {
		return reengCode;
	}

	public void setReengCode(ReengCode reengCode) {
		this.reengCode = reengCode;
	}

}
