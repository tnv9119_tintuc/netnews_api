package com.viettel.reeng.utils;

import com.google.gson.JsonObject;

public class ReengCodeDesc {

	//
	public static enum ReengCode {

		// music together room
		MUSIC_ROOM_BUSY(205), MUSIC_ROOM_TIMEOUT(206),
		/**
		 * Generate code successful
		 */
		SUCCESSFUL(200),
		// funquiz
		QUESTION_EXPIRED(422), CORRECT_ANSWER(420), INCORRECT_ANSWER(421),

		// general
		NOT_FOUND(404), BAD_REQUEST_ERROR(400), NOT_AUTHORIZED(401), TOKEN_EXPIRED(419),
		/**
		 * Error: username is not phone number 405 not-allow
		 */
		ERROR_INVALID_USERNAME(405),

		/**
		 * Error: username is not phone number 405 not-allow
		 */
		ERROR_FORBIDDEN_USERNAME(403),

		/**
		 * Error: username:
		 */
		ERROR_BLOCKED_ACCESS(451),

		/**
		 * Error: internal error
		 */
		ERROR_INTERNAL(500),

		/**
		 * Error: Method not supported
		 */
		ERROR_VERSION_NOT_SUPPORTED(505),
		/**
		 * Error: username is locked 550 permission denied
		 */
		ERROR_LOCKED_USERNAME(550);

		private int value;

		ReengCode(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public String getDesc() {
			String result;
			switch (value) {
			case 200:
				result = "Successful";
				break;
			case 205:
				result = "Busy";
				break;
			case 206:
				result = "Timeout";
				break;
			case 400:
				result = "bad request";
				break;
			case 401:
				result = "not authorized";
				break;
			case 403:
				result = "Forbidden";
				break;
			case 405:
				result = "Username is not phone number";
				break;
			case 404:
				result = "User not found";
				break;
			case 410:
				result = "gone method";
				break;
			case 419:
				result = "token expired";
				break;
			case 420:
				result = "correct answer";
				break;
			case 421:
				result = "incorrect answer";
				break;
			case 422:
				result = "question expired";
				break;
			case 451:
				result = "Blocked access";
				break;
			case 500:
				result = "Internal error";
				break;
			case 505:
				result = "Not supported";
				break;
			case 550:
				result = "Username is locked";
				break;
			default:
				result = "Error";

			}

			return result;

		}

		@Deprecated
		public JsonObject getJsonObj() {
			JsonObject obj = new JsonObject();

			obj.addProperty("errorCode", this.value);
			obj.addProperty("desc", this.getDesc());

			return obj;
		}

		@Deprecated
		public String getJsonString() {
			return getJsonObj().toString();
		}

		// tmp
		public JsonObject getOtpJsonObj() {
			JsonObject obj = new JsonObject();

			obj.addProperty("code", this.value);
			obj.addProperty("desc", this.getDesc());
			obj.addProperty("errorCode", this.value);
			return obj;
		}

		public JsonObject getOtpJsonObj(String description) {
			JsonObject obj = new JsonObject();

			obj.addProperty("code", this.value);
			obj.addProperty("desc", description);
			obj.addProperty("errorCode", this.value);
			return obj;
		}

		public String getOtpJsonString() {
			return getOtpJsonObj().toString();
		}

		public String getOtpJsonString(String description) {
			return getOtpJsonObj(description).toString();
		}
	}

}
