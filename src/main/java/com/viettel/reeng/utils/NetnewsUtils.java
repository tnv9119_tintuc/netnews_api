package com.viettel.reeng.utils;

import com.viettel.reeng.config.Constants;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class NetnewsUtils {

    public static String getTinBaiDetail(String parentID, String cid, String ID, String title)
    {
        try
        {
            if (Constants.isVip == "0")
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
            else
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
        }
        catch(Exception ex)
        {
            if (Constants.isVip == "0")
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
            else
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
        }
    }

    public static String getNiceUrl(Object objurl)
    {
        try
        {
            String url = objurl.toString();
            String niceurl = convertENRegex(url);
            //Replace những kí tự không phải 0-9, a-z, A-Z bằng khoảng trắng
            niceurl = niceurl.replaceAll("[^0-9a-zA-Z]+", " ");
            //Replace khoảng trắng bằng dấu "-"
            niceurl = niceurl.replaceAll("\\s+", "-");

            //niceurl = removeChar(niceurl, new String[] { "/", "m²", ":", ",", "<", ">", "”", "“", ".", "!", "?", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "~", "`", "\"" });
            //return niceurl;
            int indexLast = niceurl.lastIndexOf("-");
            //trường hợp có ký tự đặc biệt ở cuối title ==> sẽ thừa 1 dấu -
            if (indexLast == niceurl.length() - 1 && niceurl.length() > 0)
                niceurl = niceurl.substring(0, niceurl.length() - 1);
            return niceurl;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    protected static String getAliasCM(String pid)
    {
        if (pid == "0")
            return "-Home";
        if (pid == "1")
            return "-xa-hoi";
        if (pid == "3")
            return "-giai-tri";
        if (pid == "5")
            return "-phap-luat";
        if (pid == "6")
            return "-kinh-doanh";
        if (pid == "7")
            return "-nguoi-dep";
        if (pid == "10")
            return "-the-thao";
        if (pid == "29")
            return "-chuyen-la";
        if (pid == "135")
            return "-Video";
        if (pid == "149")
            return "-Cmcuoi";
        if (pid == "150")
            return "-quan-su";
        if (pid == "151")
            return "-audio";
        if (pid == "166")
            return "-thong-tin-dich-vu";
        if (pid == "194")
            return "-toan-canh-su-kien";
        if (pid == "280")
            return "-nong";
        if (pid == "312")
            return "-thien-nguyen";
        if (pid == "482")
            return "-tien-ich";
        if (pid == "542")
            return "-loi-xin-loi-muon";
        if (pid == "560")
            return "-tam-su";
        if (pid == "1488")
            return "-the-gioi";
        if (pid == "728")
            return "-nha-nong";
        if (pid == "313")
            return "-cong-nghe";
        if (pid == "416")
            return "-Xe";
        if (pid == "1540")
            return "-du-lich";
        return "-home";
    }

    public static String convertENRegex(String sCharacter)
    {
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        String temp = Normalizer.normalize(sCharacter, Normalizer.Form.NFD);
        return pattern.matcher(temp).replaceAll("").replaceAll("đ", "d").replaceAll("Đ", "D");
    }

    public static String genRewriteURLImage(String lead_image, long id)
    {
        String link_image = "";

        try
        {
            if (lead_image.indexOf("http") != -1)
            {
                link_image = lead_image;
            }
            else
            {
                if (id == 0)
                {
                    link_image = Utils.sWebImageVip + lead_image;
                }
                else
                {
                    if (id >= Utils.Max_id_img_old)
                        link_image = Utils.sWebImageVipNew  + lead_image;
                    else
                        link_image = Utils.sWebImageVip + lead_image;
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        return link_image;
    }

    public static String genRewriteURLImageEvent(String lead_image, int id)
    {
        String link_image = "";

        try
        {

            link_image = lead_image.replace("archive/imageslead/", Utils.sWebImageVipNew + "archive/imageslead/");
            link_image = link_image.replace("/http://", "http://");

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        return link_image;
    }

    public static String genRewriteURLImage(String lead_image, long id, int vip)
    {
        String link_image = "";

        try
        {
            if (lead_image.toLowerCase().indexOf("http") != -1)
            {
                link_image = lead_image;
            }
            else
            {
                if (vip == 1)
                {
                    if (id == 0)
                    {
                        link_image = Utils.sWebImageVip + lead_image;
                    }
                    else
                    {
                        if (id >= Utils.Max_id_img_old)
                            link_image = Utils.sWebImageVipNew + lead_image;
                        else
                            link_image = Utils.sWebImageVip + lead_image;
                    }
                }
                else
                {
                    if (id == 0)
                    {
                        link_image = Utils.sWebImage + lead_image;
                    }
                    else
                    {
                        if (id >= Utils.Max_id_img_old)
                            link_image = Utils.sWebImageNew + lead_image;
                        else
                            link_image = Utils.sWebImage + lead_image;
                    }
                }
            }
        }
        catch(Exception ex)
        {

        }

        return link_image;
    }

    public static String genRewriteUrlVideo(String leadImage, long id, int vip)
    {
        String link_image = "";

        try
        {
            if (leadImage.toLowerCase().indexOf("http") != -1)
            {
                link_image = leadImage;
            }
            else
            {
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= id && leadImage.contains(".mp4"))
                {
                    if (vip == 1)
                        link_image = Constants.video_cdn + leadImage;
                    else
                        link_image = Constants.video_cdn_normal + leadImage;
                }
                else
                {
                    if (vip == 1)
                    {
                        if (id == 0)
                        {
                            link_image = Utils.sWebImageVip + leadImage;
                        }
                        else
                        {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageVipNew + leadImage;
                            else
                                link_image = Utils.sWebImageVip + leadImage;
                        }
                    }
                    else
                    {
                        if (id == 0)
                        {
                            link_image = Utils.sWebImage + leadImage;
                        }
                        else
                        {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageNew + leadImage;
                            else
                                link_image = Utils.sWebImage + leadImage;
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //
        }

        return link_image;
    }


    public static  String getMediaUrl (String mediaUrl,long id,int vip)
    {
        String media_url = "";
        try{
            if (media_url.toLowerCase().indexOf(".mp3") > 0)
            {
                media_url = genRewriteURLImage(media_url, id, vip);
            }
            else if (media_url.toLowerCase().indexOf(".mp4") > 0)
            {
                media_url = genRewriteUrlVideo(media_url, id, vip);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  media_url;
    }

    public static  String[] getSourceAndIconName (String source)
    {
        String sourceName = "";
        String sourceIcon = "";
        try
        {
            if ((source.indexOf(",") != -1) && (source.length() > 40))
            {
                String[] Sources = source.split(",");

                sourceName = Sources[0].toString();
                sourceIcon =  Constants.ImageDBPathVIP + Sources[1].toString();
            }
            else
            {
                sourceName = source;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return new String[] {sourceName,sourceIcon};

    }

}
