package com.viettel.reeng.utils;

/**
 * ********************************************************\ | | | XXTEA.java |
 * | | | XXTEA encryption algorithm library for Java. | | | | Encryption
 * Algorithm Authors: | | David J. Wheeler | | Roger M. Needham | | | | Code
 * Authors: Ma Bingyao <mabingyao@gmail.com> | | LastModified: Mar 10, 2015 | |
 * | \*********************************************************
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xxtea.XXTEA;

import java.io.UnsupportedEncodingException;

public class XXTea {

    private static final int DELTA = 0x9E3779B9;
    private static final Logger logger = LogManager.getLogger(XXTea.class);

    private static int MX(int sum, int y, int z, int p, int e, int[] k) {
        return (z >>> 5 ^ y << 2) + (y >>> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
    }

    private XXTea() {
    }

    public static final byte[] encrypt(byte[] data, byte[] key) {
        if (data.length == 0) {
            return data;
        }
        return toByteArray(encrypt(toIntArray(data, true), toIntArray(fixKey(key), false)), false);
    }

    public static final byte[] encrypt(String data, byte[] key) {
        try {
            return encrypt(data.getBytes("UTF-8"), key);
        } catch (Exception e) {
            logger.error("Exception", e);
            return null;
        }
    }

    public static final byte[] encrypt(byte[] data, String key) {
        try {
            return encrypt(data, key.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.info("[Exception ]", e);
            return null;
        }
    }

    public static final byte[] encrypt(String data, String key) {
        try {
            return encrypt(data.getBytes("UTF-8"), key.getBytes("UTF-8"));
        } catch (Exception e) {
            logger.error("Exception", e);
            return null;
        }
    }

    public static final String encryptToBase64String(byte[] data, byte[] key) {
        byte[] bytes = encrypt(data, key);
        return Base64.encode(bytes);
    }

    public static final String encryptToBase64String(String data, byte[] key) {
        byte[] bytes = encrypt(data, key);
        if (bytes == null) {
            return null;
        }
        return Base64.encode(bytes);
    }

    public static final String encryptToBase64String(byte[] data, String key) {
        byte[] bytes = encrypt(data, key);
        if (bytes == null) {
            return null;
        }
        return Base64.encode(bytes);
    }

    public static final String encryptToBase64String(String data, String key) {

          return XXTEA.encryptToBase64String(data, key);
//        byte[] bytes = encrypt(data, key);
//        if (bytes == null) {
//            return null;
//        }
//        return Base64.encode(bytes);
    }

    public static final byte[] decrypt(byte[] data, byte[] key) {
        try {
            if (data.length == 0) {
                return data;
            }
            return toByteArray(decrypt(toIntArray(data, false), toIntArray(fixKey(key), false)), true);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error("Exception", e);
            return data;
        }
    }

    public static final byte[] decrypt(byte[] data, String key) {
        try {
            return decrypt(data, key.getBytes("UTF-8"));
        } catch (Exception e) {
            logger.error("Exception", e);
            return null;
        }
    }

    public static final byte[] decryptBase64String(String data, byte[] key) {
        return decrypt(Base64.decode(data), key);
    }

    public static final byte[] decryptBase64String(String data, String key) {
        return decrypt(Base64.decode(data), key);
    }

    public static final String decryptToString(byte[] data, byte[] key) {
        try {
            byte[] bytes = decrypt(data, key);
            if (bytes == null) {
                return null;
            }
            return new String(bytes, "UTF-8");
        } catch (Exception ex) {
            logger.error("Exception", ex);
            return null;
        }
    }

    public static final String decryptToString(byte[] data, String key) {
        try {
            byte[] bytes = decrypt(data, key);
            if (bytes == null) {
                return null;
            }
            return new String(bytes, "UTF-8");
        } catch (Exception ex) {
            logger.error("Exception", ex);
            return null;
        }
    }

    public static final String decryptBase64StringToString(String data, byte[] key) {
        try {
            byte[] bytes = decrypt(Base64.decode(data), key);
            if (bytes == null) {
                return null;
            }
            return new String(bytes, "UTF-8");
        } catch (Exception ex) {
            logger.error("Exception", ex);
            return null;
        }
    }

    public static final String decryptBase64StringToString(String data, String key) {
        try {
            byte[] bytes = decrypt(Base64.decode(data), key);
            if (bytes == null) {
                return null;
            }
            return new String(bytes, "UTF-8");
        } catch (Exception ex) {
            logger.error("Exception", ex);
            return null;
        }
    }

    private static int[] encrypt(int[] v, int[] k) {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        int p, q = 6 + 52 / (n + 1);
        int z = v[n], y, sum = 0, e;

        while (q-- > 0) {
            sum = sum + DELTA;
            e = sum >>> 2 & 3;
            for (p = 0; p < n; p++) {
                y = v[p + 1];
                z = v[p] += MX(sum, y, z, p, e, k);
            }
            y = v[0];
            z = v[n] += MX(sum, y, z, p, e, k);
        }
        return v;
    }

    private static int[] decrypt(int[] v, int[] k) {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        int p, q = 6 + 52 / (n + 1);
        int z, y = v[0], sum = q * DELTA, e;

        while (sum != 0) {
            e = sum >>> 2 & 3;
            for (p = n; p > 0; p--) {
                z = v[p - 1];
                y = v[p] -= MX(sum, y, z, p, e, k);
            }
            z = v[n];
            y = v[0] -= MX(sum, y, z, p, e, k);
            sum = sum - DELTA;
        }
        return v;
    }

    private static byte[] fixKey(byte[] key) {
        if (key.length == 16) {
            return key;
        }
        byte[] fixedkey = new byte[16];
        if (key.length < 16) {
            System.arraycopy(key, 0, fixedkey, 0, key.length);
        } else {
            System.arraycopy(key, 0, fixedkey, 0, 16);
        }
        return fixedkey;
    }

    private static int[] toIntArray(byte[] data, boolean includeLength) {
        int n = (((data.length & 3) == 0) ? (data.length >>> 2) : ((data.length >>> 2) + 1));
        int[] result;

        if (includeLength) {
            result = new int[n + 1];
            result[n] = data.length;
        } else {
            result = new int[n];
        }
        n = data.length;
        for (int i = 0; i < n; ++i) {
            result[i >>> 2] |= (0x000000ff & data[i]) << ((i & 3) << 3);
        }
        return result;
    }

    private static byte[] toByteArray(int[] data, boolean includeLength) {
        int n = data.length << 2;

        if (includeLength) {
            int m = data[data.length - 1];
            n -= 4;
            if ((m < n - 3) || (m > n)) {
                return null;
            }
            n = m;
        }
        byte[] result = new byte[n];

        for (int i = 0; i < n; ++i) {
            result[i] = (byte) (data[i >>> 2] >>> ((i & 3) << 3));
        }
        return result;
    }

    public static void main(String[] args) {
        String key = "28923965891460713581941317";
        String string = "data";
        String encry = encryptToBase64String(string, key);
        byte[] bytes = encrypt(string, key);
        String base64 = new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytes));
        System.out.println("base64: " + base64 + ", size: " + base64.length());
        System.out.println("---------------------->>>encry: " + encry + ", data: " + string.length());
        String decry = decryptBase64StringToString("+YQTSFgCTON5uvnObSl3mf1UpCX66vsZAH9J75yj/CcmEoyKfljMiVWdOR2KfamPSDVFkgBhLirUj7cSM58B0R7Qvmx9hFLYgluBLg==H", "3726540151448442335751037");

        System.out.println("---------------------->>>decry: " + decry);
        byte[] de = decrypt(org.apache.commons.codec.binary.Base64.decodeBase64(base64), key);
        System.out.println("---->decry base64: " + new String(de));

        /*
		 * try { byte[] bytes =
		 * decrypt(org.apache.commons.codec.binary.Base64.decodeBase64(
		 * "+YQTSFgCTON5uvnObSl3mf1UpCX66vsZAH9J75yj/CcmEoyKfljMiVWdOR2KfamPSDVFkgBhLirUj7cSM58B0R7Qvmx9hFLYgluBLg=="
		 * ), "3726540151448442335751037");
		 * System.out.println("---------------------> 11: " + new
		 * String(org.apache.commons.codec.binary.Base64.decodeBase64(
		 * "+YQTSFgCTON5uvnObSl3mf1UpCX66vsZAH9J75yj/CcmEoyKfljMiVWdOR2KfamPSDVFkgBhLirUj7cSM58B0R7Qvmx9hFLYgluBLg=="
		 * ), "UTF-8"));
		 * 
		 * } catch (Exception ex) { ex.printStackTrace(); }
         */
    }
}
