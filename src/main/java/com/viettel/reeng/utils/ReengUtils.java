package com.viettel.reeng.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import com.viettel.reeng.service.ResourceService;
import com.viettel.security.PassTranformer;

public class ReengUtils {

	private static final Logger logger = LogManager.getLogger(ReengUtils.class);

	public static final String PHONE_PATTERN = "^0?[9][0-9][0-9][0-9]{6}$|^0?16[0-9]{8}$|^0?12[0-9]{8}$|^0?1[8-9][0-9]{8}$|^[+]?849[0-9][0-9][0-9]{6}$|^[+]?8416[0-9]{8}$|^[+]?8412[0-9]{8}$|^[+]?841[8-9][0-9]{8}$|^086";
	public static final String VIETTEL_PATTERN = "^096|^097|^098|^016|^086|^03";

	private static Pattern vtPhonePattern = Pattern.compile(VIETTEL_PATTERN);

	private static final String CAMBONIA_METFONE = "^(\\+85597)|^(\\+85588)|^(\\+85571)|^(\\+85531)|^(\\+85560)|^(\\+85566)|^(\\+85567)|^(\\+85568)|^(\\+85590)";
	private static Pattern metfonePattern = Pattern.compile(CAMBONIA_METFONE);

	private static final String MYANMAR_MYTEL = "^(\\+95969)|^(\\+95968)";
	private static Pattern mytelPattern = Pattern.compile(MYANMAR_MYTEL);

	private static final String MYANMAR = "^(\\+95)";
	private static Pattern myanmarPattern = Pattern.compile(MYANMAR);

	private static final String CAMBONIA = "^(\\+855)";
	private static Pattern cambodiaPattern = Pattern.compile(CAMBONIA);

	private static final String LAO_UNITEL = "^(\\+856209)|^(\\+856309)|^(\\+856304)";
	private static Pattern laoPattern = Pattern.compile(LAO_UNITEL);

	public static final String DESKTOP_PLATFORM = "desktop";

	public static final String MOBILE_RESOURCE = "reeng";

	public static boolean isMyanmarMytel(String number) {
		if (number == null || number.length() == 0)
			return false;

		if (mytelPattern.matcher(number).find()) {
			return true;
		}
		return false;
	}

	public static boolean isMyanmarNumber(String number) {
		if (number == null || number.length() == 0)
			return false;

		if (myanmarPattern.matcher(number).find()) {
			return true;
		}
		return false;
	}

	public static boolean isCambodiaNumber(String number) {
		if (number == null || number.length() == 0)
			return false;

		if (cambodiaPattern.matcher(number).find()) {
			return true;
		}
		return false;
	}

	public static boolean isMetfoneNumber(String number) {
		if (number == null || number.length() == 0)
			return false;

		if (metfonePattern.matcher(number).find()) {
			return true;
		}
		return false;
	}

	public static boolean isLaosNumber(String number) {
		if (number == null || number.length() == 0)
			return false;

		if (laoPattern.matcher(number).find()) {
			return true;
		}
		return false;
	}

	private static final Map<String, String> operatorMap = new HashMap<String, String>();
	static {
		operatorMap.put("01", "Mobifone");
		operatorMap.put("02", "Vinaphone");
		operatorMap.put("04", "Viettel");
		operatorMap.put("05", "Vietnammobile");
		operatorMap.put("07", "Gtel Mobile");
		operatorMap.put("08", "Dong Duong Telecom");
	}

	public static final ThreadLocal<SimpleDateFormat> ddMMyyyy = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy");
		}
	};

	public static final ThreadLocal<SimpleDateFormat> yyyyMMdd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};

	public static final ThreadLocal<SimpleDateFormat> yyyyMMdd1 = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMdd");
		}
	};

	public static final ThreadLocal<SimpleDateFormat> getFormatYyyyMMddHHmmss = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};
	public static final ThreadLocal<SimpleDateFormat> getFormatYyyyMMddHHmmssMps = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};
	public static final ThreadLocal<SimpleDateFormat> MMdd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM");
		}
	};

	public static boolean isPhoneNumber(String number) {
		// if (number == null || number.length() == 0) {
		// return false;
		// }
		//
		// if (phonePattern.matcher(number).find()) {
		// return true;
		// }
		// return false;

		return isPhoneNumberNational(number);
	}

	public static boolean isNumberTelemor(String number) {
		if (number.startsWith("+67076") || number.startsWith("+67075")) {
			return true;
		}
		return false;
	}

	public static String currentDateToString() {
		Calendar calendar = Calendar.getInstance();
		java.util.Date date = calendar.getTime();
		return yyyyMMdd.get().format(date);
	}

	public static String formatCountryCode(String countryCode) {
		if (countryCode.startsWith("00")) {
			countryCode = "+" + countryCode.substring("00".length(), countryCode.length());
		}
		return countryCode;
	}

	// 84xxx => 0xxx
	public static String formatTel2AccountId(String number) {
		return number.replaceFirst("^84|^\\+84|^0084", "0");
	}

	public static String getRegionCodeForNumber(String phonenumber) {
		try {
			if (phonenumber.startsWith("0")) {
				return "VN";
			}
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber swissNumberProto = phoneUtil.parse(phonenumber, "VN");
			return phoneUtil.getRegionCodeForNumber(swissNumberProto);
		} catch (Exception e) {
			logger.error("exception: phonenumber = " + phonenumber, e);

		}
		return "VN";
	}

	public static boolean isPhoneNumberNational(String phoneNumber) {
		try {
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			String phoneWithCountryCode = formatVnMsisdnWithCountryCode(phoneNumber);
			PhoneNumber swissNumberProto = phoneUtil.parse(phoneWithCountryCode, "VN");
			return phoneUtil.isValidNumber(swissNumberProto);
		} catch (Exception e) {
			return false;
		}

	}

	public static String formatVnMsisdnWithCountryCode(String msisdn) {
		try {
			return msisdn.replaceFirst("^84|^0084|^0", "+84");
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Exception|", e);
		}
		return null;

	}

	public static void main(String[] args) {
		try {
			// String msisdn = "+821020500892";
			// String friend = "1020500894";
			// PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			// String countryMsisn = ReengUtils.getRegionCodeForNumber(msisdn);
			// System.out.println("XXXXXXXXXXXXXXX:" + countryMsisn);
			// int msisdnCountry =
			// phoneUtil.getCountryCodeForRegion(countryMsisn);
			// System.out.println("XXXXXXXXXXXXXXX:msisdnCountry=" +
			// msisdnCountry);
			// if (!friend.startsWith("+")) {
			// friend = friend.replaceFirst("0", "");
			// friend = "+" + msisdnCountry + "" + friend;
			// }
			// System.out.println("XXXXXXXXXXXXXXX:msisdnCountry=" + friend +
			// "|" + ReengUtils.isPhoneNumber(friend));
			// System.out.println("XXXXXXXXXXXXXXX:" +
			// isPhoneNumberNational("+8562090670844"));

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

	}

	public static String removeAccent(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replaceAll("đ", "d");
	}

	/**
	 * generate random code
	 */
	public static String genCode(int min, int max) throws Exception {
		/*
		 * int code = 0; String strCode = null; code = (int) (min +
		 * Math.random() * (max - min)); strCode = Integer.toString(code);
		 * return strCode;
		 */
		try {
			return genarateCode(min, max);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(e);
		}
	}

	public static String genarateCode(int min, int max) throws Exception {
		try {
			SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
			String randomNum = new Integer(prng.nextInt(max - min + 1) + min).toString();
			return randomNum;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Unable to generate code");
			throw new Exception(e);
		}

	}

	public static final ThreadLocal<SimpleDateFormat> hhmm = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("HH:mm");
		}
	};

	public static int startService(String startTime, String endTime) {
		String[] startTimes = startTime.split(",");
		String[] endTimes = endTime.split(",");
		String[] currentStrings = hhmm.get().format(new java.util.Date()).split(":");
		for (int i = 0; i < startTimes.length; i++) {
			String[] startStrings = startTimes[i].trim().split(":");
			String[] endStrings = endTimes[i].trim().split(":");
			String start = StringUtils.join(startStrings);
			String end = StringUtils.join(endStrings);
			String current = StringUtils.join(currentStrings);
			if (Integer.parseInt(current) >= Integer.parseInt(start)
					&& Integer.parseInt(current) <= Integer.parseInt(end)) {
				return 0;
			}
			if (Integer.parseInt(current) < Integer.parseInt(start)) {
				return -1;
			}
		}
		return 1;
	}

	// convert from internal Java String format -> UTF-8
	// public static String convertToUTF8(String s) {
	// String out = null;
	// try {
	// out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
	// } catch (java.io.UnsupportedEncodingException e) {
	// return null;
	// }
	// return out;
	// }
	public void byteToFile(byte[] bytes, String filePath, String fileName) {
		BufferedOutputStream stream = null;
		FileOutputStream fileOutputStream = null;
		try {
			// Create the file on server
			File serverFile = new File(filePath + fileName);
			fileOutputStream = new FileOutputStream(serverFile);
			stream = new BufferedOutputStream(fileOutputStream);
			stream.write(bytes);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
				if (stream != null) {
					stream.close();
				}
			} catch (Exception ex) {
				logger.error("ex", ex);
			}
		}

	}

	public static String encryptSHA256(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			return hexString.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String encryptMD5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());

			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		} catch (EnumConstantNotPresentException e) {
			logger.error(e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}

	public static byte[] toDigest(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(Bytes.toBytes(s));
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public static long MbtoBye(int Mb) {
		return Mb * (1024 * 1024);

	}

	public static int generatedIntegerString(int length) {
		Random number = new Random();
		char[] digits = new char[length];// length 15
		digits[0] = (char) ('1' + number.nextInt(9));
		for (int i = 1; i < length; i++) {
			digits[i] = (char) ('0' + number.nextInt(10));
		}
		return Integer.parseInt(new String(digits));
	}

	public static boolean isVtNumberTel(String number) {
		if (number == null || number.length() == 0) {
			return false;
		}

		if (vtPhonePattern.matcher(number).find()) {
			return true;
		}
		return false;
	}
	//
	// public static String createOtpSms(String to, String from, String subject,
	// String content) {
	// return null;
	// }

	public static boolean isNotEmpty(String string) {
		return string != null && !("").equals(string);
	}

	public static String toJson(Map<String, Object> map) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(map);

	}

	// public static String listToJsonArray(List<Object> lsObject) {
	// Type listType = new TypeToken<List<Object>>() {
	// }.getType();
	// Gson gson = new Gson();
	// JsonElement js = gson.toJsonTree(lsObject, listType);
	// return js.toString();
	// }

	// Set<Map.Entry<String, String> entries = map.entrySet();
	// JSONArray mJSONArray = new JSONArray(entries);
	public static String genResult(int code, String desc) {

		JsonObject obj = new JsonObject();

		obj.addProperty("errorCode", code);
		obj.addProperty("desc", desc);

		return obj.toString();
	}

	// public static String genResult(int code, String desc,
	// Map<String,JsonElement> params) {
	//
	// JsonObject obj = new JsonObject();
	//
	// obj.addProperty("errorCode", code);
	// obj.addProperty("desc", desc);
	// for(String key: params.keySet()){
	// obj.add(key, params.get(key));
	// }
	//
	// return obj.toString();
	// }
	// public static String genResult(int code, String desc, Map<String, Object>
	// params) {
	//
	// JsonObject obj = new JsonObject();
	//
	// obj.addProperty("errorCode", code);
	// obj.addProperty("desc", desc);
	// for (String key : params.keySet()) {
	// Object value = params.get(key);
	// if (value instanceof JsonElement) {
	// obj.add(key, (JsonElement) params.get(key));
	// } else {
	// if (value instanceof String) {
	// obj.addProperty(key, (String) params.get(key));
	// } else {
	// obj.addProperty(key, (Number) params.get(key));
	// }
	// }
	// }
	//
	// return obj.toString();
	// }

	// public static String getEncrypSHA(String stringData) {
	//
	// try {
	// MessageDigest md = MessageDigest.getInstance("SHA-256");
	// md.update(stringData.getBytes());
	//
	// byte byteData[] = md.digest();
	//
	// // convert the byte to hex format method 1
	// StringBuffer sb = new StringBuffer();
	// for (int i = 0; i < byteData.length; i++) {
	// sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,
	// 16).substring(1));
	// }
	// return sb.toString();
	// } catch (NoSuchAlgorithmException e) {
	// // _log.error("Failed to encrypt password.", e);
	// logger.error("Exception|", e);
	// }
	// return "";
	// }

	// public static String genOtpResult(OtpCode code,long lockedTime) {
	//
	// JsonObject obj = new JsonObject();
	//
	// obj.addProperty("code", code.getValue());
	// obj.addProperty("desc", code.getDesc());
	// if(lockedTime != 0){
	// obj.addProperty("lockedTimeSd", lockedTime);
	// }
	// return obj.toString();
	// }
	//
	// public static String genOtpResult(OtpCode code,String desc) {
	//
	// JsonObject obj = new JsonObject();
	//
	// obj.addProperty("code", code.getValue());
	// obj.addProperty("desc", desc);
	//
	// return obj.toString();
	// }
	//
	//
	// public static String genOtpResult(OtpCode code, long lockedTime,
	// JsonArray jsonArray) {
	// JsonObject obj = new JsonObject();
	//
	// obj.addProperty("code", code.getValue());
	// obj.addProperty("desc", code.getDesc());
	// if(lockedTime != 0){
	// obj.addProperty("lockedTimeSd", lockedTime);
	// }
	// if(jsonArray != null){
	// obj.add("subscription_package", jsonArray);
	// }
	// return obj.toString();
	//
	// }
	// normaliser to 84xxx
	public static String normalizeMsisdn(String msisdn) {

		if (msisdn.startsWith("84")) {
			return msisdn;
		}

		if (msisdn.startsWith("+84")) {
			return "84" + msisdn.substring(3, msisdn.length());
		}

		if (msisdn.startsWith("0084")) {
			return "84" + msisdn.substring(4, msisdn.length());
		}

		if (msisdn.startsWith("0")) {
			return "84" + msisdn.substring(1, msisdn.length());
		}

		return "84" + msisdn;
	}

	private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz0123456789";

	public static String genRandomString(int size) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < size; i++) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public static long getTimeToLive() {
		try {
			long now = System.currentTimeMillis();
			String str = yyyyMMdd.get().format(new Date()) + " 23:59:59";
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getFormatYyyyMMddHHmmss.get().parse(str));
			return (calendar.getTimeInMillis() - now) / 1000;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("", e);
		}
		return 6 * 60 * 60;
	}

	// public static int getDaysFromMiliseconds(long start, long end) {
	// return (int) ((end - start) / 1000 / 60 / 60 / 24);
	// }

	public static int stringToInt(String str, int def) {
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Exception|", e);
			return def;
		}
	}

	// public static void saveToLog(String mlog) {
	// common.fatal(mlog);
	// }

	public static String getContentOfLanguageCode(String key, String languageCode, ResourceService resourceService) {
		String language = "vi";
		try {
			if (!"vi".equalsIgnoreCase(languageCode)) {
				language = "en";
			}
			return resourceService.getMessage(key, language, "VN");
		} catch (Exception e) {
			logger.error("getContentOfCountryCode have Exception:" + e.getMessage() + "|languageCode=" + languageCode,
					e);
		}
		return "";
	}

	/**
	 * SOAP Webservice: Xoa toan bo <enveelope> toi <body> de parse thanh object
	 *
	 * @param xml
	 * @return
	 */
	public static String removeEnvelopeTag(String xml) {

		String bodyOpen = "<s:body>";
		int start = xml.toLowerCase().indexOf(bodyOpen);
		int end = xml.toLowerCase().indexOf("</s:body");
		String s = xml.substring(start + bodyOpen.length(), end);
		return s;
	}

	// public static String toDateISOString(Date date) {
	// TimeZone tz = TimeZone.getDefault();
	// DateFormat df = new SimpleDateFormat(ReengConstant.DATE_ISO_FORMAT);
	// df.setTimeZone(tz);
	// return df.format(date);
	// }

	public static Date toDateISO(String dateStr) {
		DateFormat df1 = new SimpleDateFormat(ReengConstant.DATE_ISO_FORMAT);
		try {
			return df1.parse(dateStr);
		} catch (ParseException e) {
			logger.info("Parse date Error: " + e, e);
		}
		return null;
	}

	public static String formatMsisdnWithCountryCode(String msisdn) {
		if (msisdn.startsWith("84")) {
			return "+" + msisdn;
		} else if (msisdn.startsWith("+")) {
			return msisdn;
		} else if (msisdn.startsWith("0084")) {
			return "+" + msisdn.substring(2);
		} else if (msisdn.startsWith("0")) {
			return "+84" + msisdn.substring(1);
		} else {
			return "+84" + msisdn;
		}
	}

	public static String formatVirtualNumVTToCountryCallingNumber(String number, String countryCode) {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		int countryCodeForResion = phoneUtil.getCountryCodeForRegion(countryCode);
		String vnNumber = formatTel2AccountId(number);
		if (vnNumber.startsWith("0")) {
			return "+" + countryCodeForResion + vnNumber.substring(1);
		} else {
			return vnNumber;
		}
	}

	public static String getCountryCode(String dbpath, String remoteIp) {
		try {

			File database = new File(dbpath);

			DatabaseReader reader = new DatabaseReader.Builder(database).withCache(new CHMCache()).build();

			InetAddress address = InetAddress.getByName(remoteIp);

			String country_code = reader.country(address).getCountry().getIsoCode();
			String country_name = reader.country(address).getCountry().getName();

			return country_code;
		} catch (Exception ex) {

		}
		return "VN";
	}

	public static String getRegxOldNumber() {
		return "(^0120|^0121|^0122|^0126|^0128|^0123|^0124|^0125|^0127|^0129|^0162|^0163|^0164|^0165|^0166|^0167|^0168|^0169|^0186|^0188|^0199)\\d{7}";
	}

	public static String convertNumberold2new(String msisdn) {
		if (msisdn.startsWith("0120")) {
			return msisdn.replaceFirst("^0120", "070");
		} else if (msisdn.startsWith("0121")) {
			return msisdn.replaceFirst("^0121", "079");
		} else if (msisdn.startsWith("0122")) {
			return msisdn.replaceFirst("^0122", "077");
		} else if (msisdn.startsWith("0126")) {
			return msisdn.replaceFirst("^0126", "076");
		} else if (msisdn.startsWith("0128")) {
			return msisdn.replaceFirst("^0128", "078");
		} else if (msisdn.startsWith("0123")) {
			return msisdn.replaceFirst("^0123", "083");
		} else if (msisdn.startsWith("0124")) {
			return msisdn.replaceFirst("^0124", "084");
		} else if (msisdn.startsWith("0125")) {
			return msisdn.replaceFirst("^0125", "085");
		} else if (msisdn.startsWith("0127")) {
			return msisdn.replaceFirst("^0127", "081");
		} else if (msisdn.startsWith("0129")) {
			return msisdn.replaceFirst("^0129", "082");
		} else if (msisdn.startsWith("0162")) {
			return msisdn.replaceFirst("^0162", "032");
		} else if (msisdn.startsWith("0163")) {
			return msisdn.replaceFirst("^0163", "033");
		} else if (msisdn.startsWith("0164")) {
			return msisdn.replaceFirst("^0164", "034");
		} else if (msisdn.startsWith("0165")) {
			return msisdn.replaceFirst("^0165", "035");
		} else if (msisdn.startsWith("0166")) {
			return msisdn.replaceFirst("^0166", "036");
		} else if (msisdn.startsWith("0167")) {
			return msisdn.replaceFirst("^0167", "037");
		} else if (msisdn.startsWith("0168")) {
			return msisdn.replaceFirst("^0168", "038");
		} else if (msisdn.startsWith("0169")) {
			return msisdn.replaceFirst("^0169", "039");
		} else if (msisdn.startsWith("0186")) {
			return msisdn.replaceFirst("^0186", "056");
		} else if (msisdn.startsWith("0188")) {
			return msisdn.replaceFirst("^0188", "058");
		} else if (msisdn.startsWith("0199")) {
			return msisdn.replaceFirst("^0199", "059");
		}

		return msisdn;
	}

	public static String getRegxNewNumber() {
		return "(^070|^079|^077|^076|^078|^083|^084|^085|^081|^082|^032|^033|^034|^035|^036|^037|^038|^039|^056|^058|^059)\\d{7}";
	}

	public static String convertNumbernew2old(String msisdn) {
		if (msisdn.startsWith("070")) {
			return msisdn.replaceFirst("^070", "0120");
		} else if (msisdn.startsWith("079")) {
			return msisdn.replaceFirst("^079", "0121");
		} else if (msisdn.startsWith("077")) {
			return msisdn.replaceFirst("^077", "0122");
		} else if (msisdn.startsWith("076")) {
			return msisdn.replaceFirst("^076", "0126");
		} else if (msisdn.startsWith("078")) {
			return msisdn.replaceFirst("^078", "0128");
		} else if (msisdn.startsWith("083")) {
			return msisdn.replaceFirst("^083", "0123");
		} else if (msisdn.startsWith("084")) {
			return msisdn.replaceFirst("^084", "0124");
		} else if (msisdn.startsWith("085")) {
			return msisdn.replaceFirst("^085", "0125");
		} else if (msisdn.startsWith("081")) {
			return msisdn.replaceFirst("^081", "0127");
		} else if (msisdn.startsWith("082")) {
			return msisdn.replaceFirst("^082", "0129");
		} else if (msisdn.startsWith("032")) {
			return msisdn.replaceFirst("^032", "0162");
		} else if (msisdn.startsWith("033")) {
			return msisdn.replaceFirst("^033", "0163");
		} else if (msisdn.startsWith("034")) {
			return msisdn.replaceFirst("^034", "0164");
		} else if (msisdn.startsWith("035")) {
			return msisdn.replaceFirst("^035", "0165");
		} else if (msisdn.startsWith("036")) {
			return msisdn.replaceFirst("^036", "0166");
		} else if (msisdn.startsWith("037")) {
			return msisdn.replaceFirst("^037", "0167");
		} else if (msisdn.startsWith("038")) {
			return msisdn.replaceFirst("^038", "0168");
		} else if (msisdn.startsWith("039")) {
			return msisdn.replaceFirst("^039", "0169");
		} else if (msisdn.startsWith("056")) {
			return msisdn.replaceFirst("^056", "0186");
		} else if (msisdn.startsWith("058")) {
			return msisdn.replaceFirst("^058", "0188");
		} else if (msisdn.startsWith("059")) {
			return msisdn.replaceFirst("^059", "0199");
		}

		return msisdn;
	}

	public static String getNumberWithoutLeadingDigit(String msisdn) {
		msisdn = msisdn.replaceFirst(
				"^070|^079|^077|^076|^078|^083|^084|^085|^081|^082|^032|^033|^034|^035|^036|^037|^038|^039|^056|^058|^059",
				"");
		msisdn = msisdn.replaceFirst(
				"^0120|^0121|^0122|^0126|^0128|^0123|^0124|^0125|^0127|^0129|^0162|^0163|^0164|^0165|^0166|^0167|^0168|^0169|^0186|^0188|^0199",
				"");
		return msisdn;
	}

	/*
	 * 016 -> 03 03 -> 016 09 -> 09
	 */
	public static String convertNumber(String msisdn) {
		if (msisdn.startsWith("01")) {
			return convertNumberold2new(msisdn);
		} else {
			return convertNumbernew2old(msisdn);
		}
	}

	/*
	 * MNP
	 */
	public static String getOperratorName(String operatorCode) {
		return operatorMap.get(operatorCode);
	}

	/*
	 * MNP
	 */
	public static int isViettelNumber(String operatorCode) {
		if ("04".equals(operatorCode)) {
			return 1;
		}
		return 0;
	}

	/*
	 * MNP
	 */
	public static String getOperratorCode(String isdn) {
		if (!isdn.startsWith("0")) {
			isdn = "0" + isdn;
		}
		String viettel = "^016\\d{8}$|^09[678]\\d{7}$|^086\\d{7}$|^03\\d{8}$";

		String Mobifone = "^012[01268]\\d{7}$|^09[03]\\d{7}$|^089\\d{7}$|^07[09768]\\d{7}$";

		String Vinaphone = "^012[34579]\\d{7}$|^09[14]\\d{7}$|^08[345128]\\d{7}$";

		String Vietnammobile = "^018[68]\\d{7}$|^092\\d{7}$|^05[68]\\d{7}$";

		String Gtel = "^0199\\d{7}$|^099\\d{7}$|^059\\d{7}$";

		if (isdn.matches(Mobifone)) {
			return "01";
		} else if (isdn.matches(Vinaphone)) {
			return "02";
		} else if (isdn.matches(viettel)) {
			return "04";
		} else if (isdn.matches(Vietnammobile)) {
			return "05";
		} else if (isdn.matches(Gtel)) {
			return "07";
		}
		return "";
	}

	public static boolean isVNNumber(String phonenumber) {
		return "VN".equals(ReengUtils.getRegionCodeForNumber(phonenumber));
	}

	public static String makeUuid(String phonenumber) {
		return PassTranformer.encrypt(phonenumber);
	}

	// 84xxx => xxx
	public static String getMsisdn(String number) {
		return number.replaceFirst("^84|^\\+84|^0084|^0", "");
	}

	public static boolean isPlatformDesktop(String platform) {
		return DESKTOP_PLATFORM.equals(platform);
	}

	//
	// public static boolean isLao(String number) {
	// if (number == null || number.length() == 0)
	// return false;
	// if (laoPattern.matcher(number).find()) {
	// return true;
	// }
	// return false;
	// }

	/*
	 * Check revision for allow separate domain or not
	 */
	public static boolean isAllowSeparateDomain(String clientType, long revision) {
		switch (clientType.toLowerCase()) {
		case "android":
			return revision >= 15190;
		case "ios":
			return revision >= 11130;
		default:
			return false;
		}
	}

}
