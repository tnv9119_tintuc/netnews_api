package com.viettel.reeng.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author nhungnt19
 *
 */
public class ReengXmlRpcClient {

	private static final Logger log = LogManager.getLogger(ReengXmlRpcClient.class);

	private String xmlRpcUrl;
	private int connectionTimeout;
	private int replyTimeout;
	public XmlRpcClientConfigImpl config = null;
	public XmlRpcClient client = new XmlRpcClient();

	private String xmlRpcAddFuncs;
	private String xmlRpcNewUserAddFuncs;
	private String xmlRpcDelFuncs;
	private String xmlRpcDelAllFunc;
	private String xmlRpcRosterFunc;

	private void init() {
		try {
			config = new XmlRpcClientConfigImpl();
			config.setServerURL(new URL(xmlRpcUrl));
			config.setConnectionTimeout(connectionTimeout);
			config.setReplyTimeout(replyTimeout);
			client.setConfig(config);
		} catch (MalformedURLException e) {
			log.error("ReengXmlRpcClient init fail ", e);
		}
	}

	private Object execute(String command, Object[] params) {
		Object result = null;
		try {
			if (config == null) {
				init();
			}
			result = client.execute(command, params);
			log.info("[ReengXmlRpcClient] execute funcs: " + command + " result: " + result);
		} catch (Exception e) {
			log.error("[ReengXmlRpcClient] execute fail funcs = " + command, e);
		}
		return result;
	}

	// public Object addContactsXmpp(String msisdn, List<String> contacts, int
	// action) {
	//
	// log.debug("[ReengXmlRpcClient] addContactsXmpp msisidn:" + msisdn + "
	// contact
	// size:" + contacts.size()
	// + " action:" + action);
	//
	// Map<String, Object> struct = new HashMap<String, Object>();
	// struct.put("user", msisdn);
	// struct.put("contacts", contacts);
	// struct.put("action", action);
	//
	// Object[] params = new Object[]{struct};
	// Object result = execute(xmlRpcAddFuncs, params);
	// return result;
	// }

	// public Object delContacts(String msisdn, List<String> contacts) {
	//
	// log.debug("[ReengXmlRpcClient] delContacts msisidn:" + msisdn + " contact
	// size:" + contacts.size());
	//
	// Map<String, Object> struct = new HashMap<String, Object>();
	// struct.put("user", msisdn);
	// struct.put("contacts", contacts);
	//
	// Object[] params = new Object[]{struct};
	// Object result = execute(xmlRpcDelFuncs, params);
	//
	// return result;
	// }

	//
	// public Object delAllContact(String msisdn) {
	//
	// log.debug("[ReengXmlRpcClient] delAllContact msisidn:" + msisdn);
	//
	// Map<String, Object> struct = new HashMap<String, Object>();
	// struct.put("user", msisdn);
	//
	// Object[] params = new Object[]{struct};
	// Object result = execute(xmlRpcDelAllFunc, params);
	// return result;
	// }

	//
	public Object addRosteritemsForNewUser(String user, List<String> contacts) {

		log.debug("[ReengXmlRpcClient] addRosteritemsForNewUser msisdn:" + user + " contact size:" + contacts.size());

		Map<String, Object> struct = new HashMap<String, Object>();
		struct.put("user", user);
		struct.put("contacts", contacts);

		Object[] params = new Object[] { struct };
		Object result = execute(xmlRpcNewUserAddFuncs, params);
		log.info("[NEW USER] user: " + user + ", result: " + result + ", xmlRpcNewUserAddFuncs: "
				+ xmlRpcNewUserAddFuncs);
		return result;

	}

	// broadcast officialMsg from mocha
	public void broadcastOfficialMsg(List<String> lsMsisdn, String content) {
		try {
			// String msisdnNonNormalize = Utils.formatTel2AccountId(msisdn);
			Map<String, Object> struct = new HashMap<String, Object>();

			content = buildMessage(content);
			struct.put("serviceName", "Mocha");
			struct.put("message", content);
			struct.put("contacts", lsMsisdn);
			struct.put("serviceId", "support");
			struct.put("type", "offical");
			struct.put("subtype", "text");

			Object[] params = new Object[] { struct };
			Object result = execute("broadcast_offical_msg", params);

			log.info("Finish broadcast_offical_msg lsMsisdn: " + lsMsisdn + " - content: " + content + " - result = "
					+ result);

		} catch (Exception e) {
			log.error("Execute xmlRpcClient fail", e);
		}

	}

	public Object kickUserBlocked(String user) {

		Map<String, Object> struct = new HashMap<String, Object>();
		struct.put("user", user);
		struct.put("host", "reeng");

		Object[] params = new Object[] { struct };
		Object result = execute("kick_user", params);

		return result;
	}

	public Object closeXMPPSession(String user) {
		Map<String, Object> struct = new HashMap<String, Object>();
		struct.put("user", user);
		struct.put("host", "reeng");
		Object[] params = new Object[] { struct };
		Object result = execute("kick_user", params);
		return result;
	}

	private String buildMessage(String content) {
		StringBuilder sb = new StringBuilder();
		sb.append("<message id = \"");
		sb.append(ReengUtils.genRandomString(7));// id
		sb.append("\" type = \"offical\"");
		sb.append(" subtype= \"text\"");
		sb.append(">");
		sb.append("<name> Mocha </name>");
		sb.append("<officalname> Mocha </officalname>");
		sb.append("<body>");
		sb.append(content);
		sb.append("</body>");
		sb.append("</message>");
		return sb.toString();

	}

	//
	public String getXmlRpcUrl() {
		return xmlRpcUrl;
	}

	public void setXmlRpcUrl(String xmlRpcUrl) {
		this.xmlRpcUrl = xmlRpcUrl;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getReplyTimeout() {
		return replyTimeout;
	}

	public void setReplyTimeout(int replyTimeout) {
		this.replyTimeout = replyTimeout;
	}

	public String getXmlRpcAddFuncs() {
		return xmlRpcAddFuncs;
	}

	public void setXmlRpcAddFuncs(String xmlRpcAddFuncs) {
		this.xmlRpcAddFuncs = xmlRpcAddFuncs;
	}

	public String getXmlRpcDelFuncs() {
		return xmlRpcDelFuncs;
	}

	public void setXmlRpcDelFuncs(String xmlRpcDelFuncs) {
		this.xmlRpcDelFuncs = xmlRpcDelFuncs;
	}

	public String getXmlRpcDelAllFunc() {
		return xmlRpcDelAllFunc;
	}

	public void setXmlRpcDelAllFunc(String xmlRpcDelAllFunc) {
		this.xmlRpcDelAllFunc = xmlRpcDelAllFunc;
	}

	public String getXmlRpcRosterFunc() {
		return xmlRpcRosterFunc;
	}

	public void setXmlRpcRosterFunc(String xmlRpcRosterFunc) {
		this.xmlRpcRosterFunc = xmlRpcRosterFunc;
	}

	public String getXmlRpcNewUserAddFuncs() {
		return xmlRpcNewUserAddFuncs;
	}

	public void setXmlRpcNewUserAddFuncs(String xmlRpcNewUserAddFuncs) {
		this.xmlRpcNewUserAddFuncs = xmlRpcNewUserAddFuncs;
	}

}
