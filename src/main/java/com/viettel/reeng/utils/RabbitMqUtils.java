package com.viettel.reeng.utils;

import com.viettel.mocha.message.ActionButton;
import com.viettel.mocha.message.DeeplinkXmppMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

//import com.viettel.reeng.model.StickerItem;

@Component
public class RabbitMqUtils {

	private Logger logger = LogManager.getLogger(RabbitMqUtils.class);

	@Autowired
	@Qualifier(value = "rabbitTemplateNew")
	private RabbitTemplate rabbitTemplateNew;

	@Autowired
	@Qualifier(value = "template")
	private RabbitTemplate rabbitTemplate;

	public void sendMessage2Queue(String queueName, String message) {
		try {
			MessageProperties prop = MessagePropertiesBuilder.newInstance()
					.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN).build();
			rabbitTemplate.send(queueName, new Message(message.getBytes(), prop));
			logger.info("Push to queue: " + queueName + " - message: " + message);
		} catch (Exception e) {
			logger.error("Push to queue fail: " + queueName + " - message: " + message, e);
		}

	}

	public void sendMessage2Queue139(String queueName, String message) {
		try {
			MessageProperties prop = MessagePropertiesBuilder.newInstance()
					.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN).build();

			rabbitTemplateNew.send(queueName, new Message(message.getBytes(), prop));
			logger.info("Push to queue139: " + queueName + " - message: " + message);
		} catch (Exception e) {
			logger.error("Push to queue fail: " + queueName + " - message: " + message, e);
		}

	}

	// nhung

	// <message from=\'0974088155@reeng/reeng\' to=\'support@offical.reeng\'
	// type=\'offical\' subtype=\'text\' id=\''+str(i)+'\'>
	// <no_store /><body>test</body></message>
	// timesend='1426254584461'
	public String buildOfficalXmppMsg(String from, String to, String type, String body, long expiredTimestamp,
			String name, Boolean isNoStore) {
		StringBuilder sb = new StringBuilder();
		sb.append("<message from=''");
		sb.append(from);
		sb.append("'' to=''");
		sb.append(to);
		// sb.append("'' contenttype=''");
		// sb.append("birthday");
		sb.append("'' timesend=''");
		sb.append(System.currentTimeMillis());
		sb.append("'' expired=''");
		sb.append(expiredTimestamp);
		sb.append("'' type=''");
		sb.append(type);
		sb.append("'' subtype=''text'' id=''");
		sb.append(ReengUtils.genRandomString(10));
		sb.append("''>");
		if (isNoStore) {
			sb.append("<no_store />");
		}

		if (name != null && !name.trim().isEmpty()) {
			sb.append("<name>");
			sb.append(name);
			sb.append("</name>");
		}

		if (name != null && !name.trim().isEmpty()) {
			sb.append("<officalname>");
			sb.append(name);
			sb.append("</officalname>");
		}

		sb.append("<body>");
		sb.append(StringUtils.escapeForXML(body));
		sb.append("</body></message>");

		return sb.toString();
	}

	// public static void main(String[] args) {
	// String msg = buildOfficalXmppMsg("support@offical.reeng/reeng",
	// "{0}@reeng/reeng", "offical",Config.BIRTHDAY_CONTENT, new
	// Date().getTime(),"Mocha",false);
	// msg = MessageFormat.format(msg, "0969651186");
	// System.out.println(msg);
	// }

	// ban tin sinh nhat gui tu A den B
	// <message id="B0uJqwn0Y5-13" to="A@reeng" from="B@reeng" type="chat"
	// subtype="greeting_voicesticker">
	// <itemid>3</itemid>
	// <packageid>4</packageid>
	// <body>chuc mung sinh nhat</body>
	// <item_type>gif</item_type>
	// <item_image_url>/sticker/1.gif</item_image_url>
	// <item_voice_url>/sticker/1.mp3</item_voice_url>
	// <officalname>XXX</officalname>
	// </message>

	// build bản tin presence

	// <presence timereceive='1436518100451' from='01652991793@reeng/reeng'
	// to='01652991793@reeng/reeng' id='XAaVN-4'
	// subtype='change_avatar'><avatar>316169354584</avatar></presence>

	public String buildXmppPresenceMsg(String from, String to, long lavatar) {
		Date now = new Date();
		StringBuilder sb = new StringBuilder();
		sb.append("<presence timereceive='");
		sb.append(now.getTime());
		sb.append("' from='");
		sb.append(from + "@reeng/reeng");
		sb.append("' to='");
		sb.append(to + "@reeng/reeng");
		sb.append("' subtype='change_avatar' id='");
		sb.append(ReengUtils.genRandomString(10));
		sb.append("'>");
		sb.append("<avatar>");
		sb.append(String.valueOf(lavatar));
		sb.append("</avatar></presence>");

		return sb.toString();
	}

	public String buildXmppStatusPresenceMsg(String from, String to, String status) {
		Date now = new Date();
		StringBuilder sb = new StringBuilder();
		sb.append("<presence timereceive='");
		sb.append(now.getTime());
		sb.append("' from='");
		sb.append(from + "@reeng/reeng");
		sb.append("' to='");
		sb.append(to + "@reeng/reeng");
		sb.append("' subtype='change_status' id='");
		sb.append(ReengUtils.genRandomString(10));
		sb.append("'>");
		sb.append("<status><![CDATA[");
		sb.append(status);
		sb.append("]]></status></presence>");
		return sb.toString();
	}

	public String buildXmppPermissionPresenceMsg(String from, String to, String permission) {
		Date now = new Date();
		StringBuilder sb = new StringBuilder();
		sb.append("<presence ");
		sb.append("timereceive='" + now.getTime() + "' ");
		sb.append("from='" + from + "@reeng/reeng' ");
		sb.append("to='" + to + "@reeng/reeng' ");
		sb.append("subtype='change_permission' ");
		sb.append("id='" + ReengUtils.genRandomString(10));
		sb.append("'>");
		{
			sb.append("<permission><![CDATA[" + permission + "]]></permission>");
		}
		sb.append("</presence>");
		return sb.toString();
	}

	public String buildXmppPresenceMusicTogether(String msisdnSub, String roomInfo) {
		String msg = "<presence id='" //
				+ ReengUtils.genRandomString(10) + "' "//
				+ "type='subscribe' " //
				+ "subtype='music_info' " //
				+ "from='" //
				+ msisdnSub //
				+ "@reeng/reeng' " //
				+ "to='" //
				+ msisdnSub + "@reeng/reeng'> " + "<room><![CDATA[{\"info\":" + roomInfo + "}]]></room> "
				+ "</presence>";

		return msg;
	}

	public static String buildDeepLink(String user, String body, String label, String deepLink) {
		DeeplinkXmppMessage deeplinkXmpp = new DeeplinkXmppMessage();
		deeplinkXmpp.setFrom("support", "offical.reeng", "reeng");
		deeplinkXmpp.setTo(user, "reeng", "reeng");
		deeplinkXmpp.setType("offical");
		deeplinkXmpp.setSubtype("deeplink");
		deeplinkXmpp.setNoStore(false);
		deeplinkXmpp.setNotifiable(false);
		deeplinkXmpp.setName("MOCHA");
		deeplinkXmpp.setOfficalName("MOCHA");
		deeplinkXmpp.setBody(body);
		deeplinkXmpp.setDeeplinkLeftButton(new ActionButton(label, deepLink));
		return deeplinkXmpp.toString();
	}

	public static String buildMochaOfficalXmppMsg(String to, String content) {
		StringBuilder sb = new StringBuilder();
		sb.append("<message ");
		sb.append("id='" + ReengUtils.genRandomString(10) + "' ");
		sb.append("from='support@offical.reeng/reeng' ");
		sb.append("to='" + to + "@reeng/reeng' ");
		sb.append("type='offical' ");
		sb.append("subtype='text' ");
		sb.append("timesend='" + System.currentTimeMillis() + "' ");
		sb.append(">");

		sb.append("<name>Mocha</name>");
		sb.append("<officalname>Mocha</officalname>");
		sb.append("<body><![CDATA[" + content + "]]></body>");

		sb.append("</message>");

		return sb.toString();
	}

}
