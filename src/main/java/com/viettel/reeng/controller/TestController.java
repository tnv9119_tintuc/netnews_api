package com.viettel.reeng.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.viettel.reeng.config.ReengCodeDesc;
import com.viettel.reeng.dao.sqlserver.NetNewsDao;
import com.viettel.reeng.dao.sqlserver.entity.Categories;
import com.viettel.reeng.entity.News;
import com.viettel.reeng.json.ResponseData;
import com.viettel.reeng.json.ResponseObject;
import com.viettel.reeng.service.AuthenticationService;
import com.viettel.reeng.service.RSAService;
import com.viettel.reeng.service.RedisService;
import com.viettel.reeng.utils.ReengCodeDesc.ReengCode;
import com.viettel.reeng.utils.ReengUtils;
import com.viettel.reeng.utils.XXTea;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConverterNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/test")
@Controller
class TestController {
	private static final Logger logger = LogManager.getLogger(TestController.class);

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private RedisService redisService;

	@Autowired
	private RSAService rsaService;

	private final String LOG_PFIX = "[ACCUMULATE]";
	@Autowired
	private NetNewsDao netNewsDao;

	//
	@RequestMapping(value = "/t1", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String t1(@RequestParam("msisdn") String msisdn, HttpServletRequest httpServletRequest) {
		logger.info(LOG_PFIX + "|LIST|msisdn=" + msisdn);

		String result = "";
		JsonObject obj = new JsonObject();
		try {
			long start = System.currentTimeMillis();

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Ex:" + e.getMessage(), e);
		}
		return "Hello";
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getAll(@RequestParam("msisdn") String msisdn) {
		List<Categories> resultlist = new ArrayList<Categories>();
		String result = "";
		try {
			resultlist = netNewsDao.getCategories();
			if (resultlist != null) {
				ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
				ResponseData resData = new ResponseData();
				resData.setCategoriesList(resultlist);
				res.setData(resData);
				result = new Gson().toJson(res);
			}
			return result;
		} catch (Exception e) {
			logger.info("Exception:" + e.getMessage(), e);
			return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
		}
	}



}
