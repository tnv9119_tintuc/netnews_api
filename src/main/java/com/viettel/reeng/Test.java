package com.viettel.reeng;

import com.google.api.client.util.DateTime;
import com.viettel.reeng.utils.NetNewsUtilsDB;
import org.jfree.data.time.Day;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Test {

    public static void main(String[] args){

        String sDate = "2020-01-31 06:31:16";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date datetime = null;
        Date now = new Date();
        try {
            Date datePub = formatter.parse("2019-10-29 12:14:46.000");
            //Timestamp timestamp = new java.sql.Timestamp(datePub.getTime());

            System.out.println(   datePub.getTime());
            //System.out.println(   dTtoUnixTS(timestamp));
            System.out.println(   dTtoUnixTS(datePub));

            Date d1 = new SimpleDateFormat("yyyyMMdd").parse("20380119");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);
            System.out.println("Time: " + d1.getTime());


            Date d2 = new Date();
            System.out.println(d2.getTime());
            datetime = formatter.parse(sDate);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");


            Integer value = 19000101;
            SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            Date date = originalFormat.parse(value.toString());
            //System.out.println(date);


                    Date dt = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(String.valueOf( (Long) (datetime.getTime())));
            System.out.println( (Long) (datetime.getTime()));
            System.out.println( dt);

            System.out.println(dateFormat.format(datetime).substring(0,5)  );

            System.out.println(datetime.getMinutes());


            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println(dateOnly.format(datetime.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.print(datetime);

    }

    public static String dTtoUnixTS(Date datePub)
    {
        String result = "";
        Long diff = null;
        try{
            Long date = (datePub.getTime()/1000);
            if( date > 20380119)
            {
                diff = (20380119- 19700101) + (date - 20380119);
            }
            else
            {
                diff = date - 19700101;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  String.valueOf(diff);
    }
}
