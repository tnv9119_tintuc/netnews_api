package com.viettel.reeng.entity;

import com.viettel.reeng.config.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;

public class Body {
    public int width = 0;
    public int mid = 0;
    public int type = 0;
    public String content = "";
    public int height = 0;
    public String media = "";
    public String poster = "";
    public int isNote = 0;
    public String contentType = "";

    public Body() {
    }

    public Body(int width, int type, String content, int height, String media, String poster,int mid) {
        this.width = width;
        this.type = type;
        this.content = content;
        this.height = height;
        this.media = media;
        this.poster = poster;
        if (type == 3 || type != 2)
        {
            int configCdn = Integer.parseInt(Constants.video_config_cdn);
            if (configCdn > 0 && configCdn <= mid && this.media.toLowerCase().contains(".mp4"))
            {
                this.media = this.media.replace(Constants.ImageAppVipDB, Constants.video_cdn);
            }
        }
    }

    public Body(int width, int mid, int type, String content, int height, String media, String poster, String isNote, String plaform) {
        this.width = width;
        this.type = type;
        this.content = content;
        this.height = height;
        this.media = media;
        this.poster = poster;
        this.isNote = Integer.parseInt(StringUtils.isEmpty(isNote) ? "0" : isNote) <= 0 ? 0 : 1;



        if (type == 3 || type != 2)
        {
            int configCdn = Integer.parseInt(Constants.video_config_cdn);
            if (configCdn > 0 && configCdn <= mid && this.media.toLowerCase().contains(".mp4"))
            {
                this.media = this.media.replace(Constants.ImageAppVipDB, Constants.video_cdn);
            }
        }
    }

    public Body(int width, int type, String content, int height, String media, String poster) {
        this.width = width;
        this.type = type;
        this.content = content;
        this.height = height;
        this.media = media;
        this.poster = poster;
    }

    public Body(int width, int mid, int type, String content, int height, String media, String poster, int isNote, String plaform) {
        this.width = width;
        this.mid = mid;
        this.type = type;
        this.content = content;
        this.height = height;
        this.media = media;
        this.poster = poster;
        this.isNote = isNote;

        if (type == 3 || type != 2)
        {
            int configCdn = Integer.parseInt(Constants.video_config_cdn);
            if (configCdn > 0 && configCdn <= mid && this.media.toLowerCase().contains(".mp4"))
            {
                this.media = this.media.replace(Constants.ImageAppVipDB, Constants.video_cdn);
            }
        }
    }

    public Body(int width, int type, String content, int height, String media, String poster, String isNote, String contentType) {
        this.width = width;
        this.type = type;
        this.content = content;
        this.height = height;
        this.media = media;
        this.poster = poster;
        this.isNote = Integer.parseInt(StringUtils.isEmpty(isNote) ? "0" : isNote) <= 0 ? 0 : 1;
        this.contentType = contentType;
    }

    public Body(ResultSet resultSet, int mid) {
        try {
            int type = resultSet.getInt("type");
            int indexColumn = 0;
            try {
                indexColumn = resultSet.findColumn("isNote");
            } catch (Exception e) {
                indexColumn = -1;
            }
            if (indexColumn != -1) {
                this.isNote = (!StringUtils.isEmpty(resultSet.getString("isNote")) ? resultSet.getInt("isNote") : 0) <= 0 ? 0 : 1;
            } else {
                this.isNote = 0;
            }

            try {
                indexColumn = resultSet.findColumn("typeContent");
            } catch (Exception e) {
                indexColumn = -1;
            }
            if (indexColumn != -1) {
                this.contentType = resultSet.getString("typeContent");
            } else {
                this.contentType = "";
            }

            this.width = resultSet.getInt("width");
            this.height = resultSet.getInt("height");
            this.type = resultSet.getInt("type");
            this.poster = "";
            if (type == 2)
            {
                this.content = resultSet.getString("image");
                this.media = resultSet.getString("video");
            } else if (type == 3)
            {
                this.content = resultSet.getString("image");
                String media = resultSet.getString("image");
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= mid && media.toLowerCase().contains(".mp4"))
                {
                    this.media = this.media.replace(Constants.ImageAppVipDB, Constants.video_cdn);
                }
            }
            else
            {
                this.content = resultSet.getString("Content");
                String media = resultSet.getString("image");
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= mid && media.toLowerCase().contains(".mp4"))
                {
                    this.media = this.media.replace(Constants.ImageAppVipDB, Constants.video_cdn);
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getIsNote() {
        return isNote;
    }

    public void setIsNote(int isNote) {
        this.isNote = isNote;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}

